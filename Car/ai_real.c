////////////////////////////////////////
//
//	File : ai.c
//	CoSpace Robot
//	Version 1.0.0
//	Jan 1 2016
//	Copyright (C) 2016 CoSpace Robot. All Rights Reserved
//
//////////////////////////////////////
//
// ONLY C Code can be compiled.
//
/////////////////////////////////////

#define CsBot_AI_H//DO NOT delete this line
#ifndef CSBOT_REAL
#include <windows.h>
#include <stdio.h>
#include <math.h>
#define DLL_EXPORT extern __declspec(dllexport)
#define false 0
#define true 1
#endif
//The robot ID : six chars unique CID.
//Find it from your CoSpace Robot label or CoSpace program download GUI.
//Don't write the below line into two lines or more lines.
char AI_MyID[6] = {'A','B','f','H','7','B'};


int Duration = 0;
int SuperDuration = 0;
int bGameEnd = false;
int CurAction = -1;
int CurGame = 0;
int SuperObj_Num = 0;
int SuperObj_X = 0;
int SuperObj_Y = 0;
int Teleport = 0;
int LoadedObjects = 0;
int ObjColor_1 = 0;
int ObjColor_2 = 0;
int ObjColor_3 = 0;
int ObjColor_4 = 0;
int iWaitArm = 0;
int MyState_1 = 0;
int US_Front = 0;
int US_Left = 0;
int US_Right = 0;
int IR_L3 = 0;
int IR_L2 = 0;
int IR_L1 = 0;
int IR_R1 = 0;
int IR_R2 = 0;
int IR_R3 = 0;
int IR_C_R = 0;
int IR_C_G = 0;
int IR_C_B = 0;
int TiltSensorX = 0;
int TiltSensorY = 0;
int PositionX = 0;
int PositionY = 0;
int Compass = 0;
int Time = 0;
int WheelLeft = 0;
int WheelRight = 0;
int LED_1 = 0;
int AI_SensorNum = 18;

#define CsBot_AI_C//DO NOT delete this line

DLL_EXPORT void SetGameID(int GameID)
{
    CurGame = GameID;
    bGameEnd = 0;
}

DLL_EXPORT int GetGameID()
{
    return CurGame;
}

//Only Used by CsBot Dance Platform
DLL_EXPORT int IsGameEnd()
{
    return bGameEnd;
}

#ifndef CSBOT_REAL

DLL_EXPORT char* GetDebugInfo()
{
    char info[3000];
    sprintf(info, "Duration=%d;SuperDuration=%d;bGameEnd=%d;CurAction=%d;CurGame=%d;SuperObj_Num=%d;SuperObj_X=%d;SuperObj_Y=%d;Teleport=%d;LoadedObjects=%d;ObjColor_1=%d;ObjColor_2=%d;ObjColor_3=%d;ObjColor_4=%d;iWaitArm=%d;MyState_1=%d;US_Front=%d;US_Left=%d;US_Right=%d;IR_L3=%d;IR_L2=%d;IR_L1=%d;IR_R1=%d;IR_R2=%d;IR_R3=%d;IR_C_R=%d;IR_C_G=%d;IR_C_B=%d;TiltSensorX=%d;TiltSensorY=%d;PositionX=%d;PositionY=%d;Compass=%d;Time=%d;WheelLeft=%d;WheelRight=%d;LED_1=%d;",Duration,SuperDuration,bGameEnd,CurAction,CurGame,SuperObj_Num,SuperObj_X,SuperObj_Y,Teleport,LoadedObjects,ObjColor_1,ObjColor_2,ObjColor_3,ObjColor_4,iWaitArm,MyState_1,US_Front,US_Left,US_Right,IR_L3,IR_L2,IR_L1,IR_R1,IR_R2,IR_R3,IR_C_R,IR_C_G,IR_C_B,TiltSensorX,TiltSensorY,PositionX,PositionY,Compass,Time,WheelLeft,WheelRight,LED_1);
    return info;
}
 
DLL_EXPORT char* GetTeamName()
{
     return "LGX _Blue";
}

DLL_EXPORT int GetCurAction()
{
    return CurAction;
}

//Only Used by CsBot Rescue Platform
DLL_EXPORT int GetTeleport()
{
    return Teleport;
}

//Only Used by CsBot Rescue Platform
DLL_EXPORT void SetSuperObj(int X, int Y, int num)
{
    SuperObj_X = X;
    SuperObj_Y = Y;
    SuperObj_Num = num;
}
//Only Used by CsBot Rescue Platform
DLL_EXPORT void GetSuperObj(int *X, int *Y, int *num)
{
    *X = SuperObj_X;
    *Y = SuperObj_Y;
    *num = SuperObj_Num;
}

#endif ////CSBOT_REAL

DLL_EXPORT void SetDataAI(volatile int* packet, volatile int *AI_IN)
{

    int sum = 0;

    MyState_1 = AI_IN[0]; packet[0] = MyState_1; sum += MyState_1;
    US_Front = AI_IN[1]; packet[1] = US_Front; sum += US_Front;
    US_Left = AI_IN[2]; packet[2] = US_Left; sum += US_Left;
    US_Right = AI_IN[3]; packet[3] = US_Right; sum += US_Right;
    IR_L3 = AI_IN[4]; packet[4] = IR_L3; sum += IR_L3;
    IR_L2 = AI_IN[5]; packet[5] = IR_L2; sum += IR_L2;
    IR_L1 = AI_IN[6]; packet[6] = IR_L1; sum += IR_L1;
    IR_R1 = AI_IN[7]; packet[7] = IR_R1; sum += IR_R1;
    IR_R2 = AI_IN[8]; packet[8] = IR_R2; sum += IR_R2;
    IR_R3 = AI_IN[9]; packet[9] = IR_R3; sum += IR_R3;
    IR_C_R = AI_IN[10]; packet[10] = IR_C_R; sum += IR_C_R;
    IR_C_G = AI_IN[11]; packet[11] = IR_C_G; sum += IR_C_G;
    IR_C_B = AI_IN[12]; packet[12] = IR_C_B; sum += IR_C_B;
    TiltSensorX = AI_IN[13]; packet[13] = TiltSensorX; sum += TiltSensorX;
    TiltSensorY = AI_IN[14]; packet[14] = TiltSensorY; sum += TiltSensorY;
    PositionX = AI_IN[15]; packet[15] = PositionX; sum += PositionX;
    PositionY = AI_IN[16]; packet[16] = PositionY; sum += PositionY;
    Compass = AI_IN[17]; packet[17] = Compass; sum += Compass;
    Time = AI_IN[18]; packet[18] = Time; sum += Time;
    packet[19] = sum;

}
DLL_EXPORT void GetCommand(int *AI_OUT)
{
    AI_OUT[0] = WheelLeft;
    AI_OUT[1] = WheelRight;
    AI_OUT[2] = LED_1;
}
DLL_EXPORT int SetInfo(volatile int *info)
{
   ObjColor_4 = info[3];
   ObjColor_3 = info[2];
   ObjColor_2 = info[1];
   ObjColor_1 = info[0];
   return 0;
}
void MoveForward()
{
	if(Duration<=8 && Duration>=0)
	{
		WheelLeft = 3;
		WheelRight = 3;
	}
	if(SuperDuration<=8 && SuperDuration>0)
	{
		WheelLeft = 3;
		WheelRight = 3;
	}
}

void Game0()
{

    if(SuperDuration>0)
    {
        SuperDuration--;
    }
    else if(Duration>0)
    {
        Duration--;
    }
    else if(IR_L3>=0 && IR_L3<=0 && IR_L2>=0 && IR_L2<=0 && IR_L1>=0 && IR_L1<=0 && IR_R1>=0 && IR_R1<=0 && IR_R2>=0 && IR_R2<=0 && IR_R3>=0 && IR_R3<=0)
    {
        Duration = 39;
        CurAction =1;
    }
    else if(IR_L3>=0 && IR_L3<=0)
    {
        Duration = 0;
        CurAction =2;
    }
    else if(IR_R3>=0 && IR_R3<=0)
    {
        Duration = 0;
        CurAction =3;
    }
    else if(IR_L2>=0 && IR_L2<=0)
    {
        Duration = 0;
        CurAction =4;
    }
    else if(IR_R2>=0 && IR_R2<=0)
    {
        Duration = 0;
        CurAction =5;
    }
    else if(IR_L1>=0 && IR_L1<=0)
    {
        Duration = 0;
        CurAction =6;
    }
    else if(IR_R1>=0 && IR_R1<=0)
    {
        Duration = 0;
        CurAction =7;
    }
    switch(CurAction)
    {
        case 1:
            WheelLeft=0;
            WheelRight=0;
            LED_1=0;
            iWaitArm = 1;
                    
            break;
        case 2:
            WheelLeft=-3;
            WheelRight=3;
            LED_1=0;
            break;
        case 3:
            WheelLeft=3;
            WheelRight=-3;
            LED_1=0;
            break;
        case 4:
            WheelLeft=-1;
            WheelRight=3;
            LED_1=0;
            break;
        case 5:
            WheelLeft=3;
            WheelRight=-1;
            LED_1=0;
            break;
        case 6:
            WheelLeft=3;
            WheelRight=3;
            LED_1=0;
            break;
        case 7:
            WheelLeft=3;
            WheelRight=3;
            LED_1=0;
            break;
        default:
            break;
    }

}


DLL_EXPORT void OnTimer()
{
    switch (CurGame)
    {
        case 9:
            break;
        case 10:
            WheelLeft=0;
            WheelRight=0;
            LED_1=0;
            break;
        case 0:
            Game0();
            break;
        default:
            break;
    }
}

