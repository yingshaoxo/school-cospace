/****************************************Copyright(c)*****************************************************
**                            Shenzhen Yuejiang Technology Co., LTD.
**
**                                 http://www.dobot.cc
**
**--------------File Info---------------------------------------------------------------------------------
** File name:           Protocol.cpp
** Latest modified Date:2016-06-01
** Latest Version:      V1.0.0
** Descriptions:        Protocol interface
**
**--------------------------------------------------------------------------------------------------------
** Created by:          Liu Zhufu
** Created date:        2016-03-14
** Version:             V1.0.0
** Descriptions:
**--------------------------------------------------------------------------------------------------------
*********************************************************************************************************/
#include "Protocol.h"
#include <stdio.h>
#include <string.h>
#include <HardwareSerial.h>
#include "ProtocolID.h"
#include "command.h"
/*********************************************************************************************************
** Protocol buffer definition
*********************************************************************************************************/
#define RAW_BYTE_BUFFER_SIZE    256
#define PACKET_BUFFER_SIZE  4
#define SerialNum Serial3                                          //娴ｈ法鏁ゆ稉鎻掑經3 

// Serial
uint8_t gSerialTXRawByteBuffer[RAW_BYTE_BUFFER_SIZE];
uint8_t gSerialRXRawByteBuffer[RAW_BYTE_BUFFER_SIZE];
Packet gSerialTXPacketBuffer[PACKET_BUFFER_SIZE];
Packet gSerialRXPacketBuffer[PACKET_BUFFER_SIZE];

ProtocolHandler gSerialProtocolHandler;
extern uint64_t gQueuedCmdWriteIndex;
extern uint64_t gQueuedCmdCurrentIndex;
extern uint64_t gQueuedCmdIndex;
extern bool IsCmdCallBack[ProtocolMax];
extern bool  gQueuedCmdFlag;
/*********************************************************************************************************
** Function name:       ProtocolInit
** Descriptions:        Init the protocol buffer etc.
** Input parameters:    None
** Output parameters:   None
** Returned value:      None
*********************************************************************************************************/
void ProtocolInit(void)
{
    // Init Serial protocol
    RingBufferInit(&gSerialProtocolHandler.txRawByteQueue, gSerialTXRawByteBuffer, RAW_BYTE_BUFFER_SIZE, sizeof(uint8_t));
    RingBufferInit(&gSerialProtocolHandler.rxRawByteQueue, gSerialRXRawByteBuffer, RAW_BYTE_BUFFER_SIZE, sizeof(uint8_t));
    RingBufferInit(&gSerialProtocolHandler.txPacketQueue, gSerialTXPacketBuffer, PACKET_BUFFER_SIZE, sizeof(Packet));
    RingBufferInit(&gSerialProtocolHandler.rxPacketQueue, gSerialRXPacketBuffer, PACKET_BUFFER_SIZE, sizeof(Packet));
}

/*********************************************************************************************************
** Function name:       ProtocolProcess
** Descriptions:        Process the protocol
** Input parameters:    None
** Output parameters:   None
** Returned value:      None
*********************************************************************************************************/
uint32_t ProtocolProcess(void)
{
    Message message;
    MessageProcess(&gSerialProtocolHandler);
    static uint64_t QueuedCmdIndex = 0;
    //閿熸嵎鍑ゆ嫹閿熸枻鎷�
    if (RingBufferGetCount(&gSerialProtocolHandler.txRawByteQueue)) {
        uint8_t data;
        while (RingBufferIsEmpty(&gSerialProtocolHandler.txRawByteQueue) == false) {
            RingBufferDequeue(&gSerialProtocolHandler.txRawByteQueue, &data);
            SerialNum.write(data);
        }
    }
        if(MessageRead(&gSerialProtocolHandler, &message)==ProtocolNoError){
          //printf("4");
            IsCmdCallBack[message.id] = true;
#if 0
            if(message.id == ProtocolQueuedCmdCurrentIndex){
                memcpy(&gQueuedCmdCurrentIndex, &message.params, sizeof(gQueuedCmdCurrentIndex)) ;
                //printf("gQueuedCmdCurrentIndex:%d\r\n",gQueuedCmdCurrentIndex);
            }else if (message.id == ProtocolPTPCmd){
                 memcpy(&gQueuedCmdWriteIndex, &message.params, sizeof(gQueuedCmdWriteIndex)) ;
                 //printf("gQueuedCmdWriteIndex:%d\r\n",gQueuedCmdWriteIndex);
            }
#endif
#if 0
            //閹垫挸宓冮幒銉︽暪閸掓壆娈戞潻鏂挎礀閺佺増宓�
            printf("Rx message: ");
            printf("message id:%d, rw:%d, isQueued:%d, paramsLen:%d\r\n",
                    message.id, message.rw, message.isQueued, message.paramsLen);
            printf("params: ");
            //get current Index & Write Index

            for(int i=0; i<message.paramsLen; i++)
            {
                printf("%02x ", message.params[i]);
            }
            printf("\r\n");
#endif 
#if 0
            uint64_t temp;
            printf("params pointer:0x%x\r\n",&message.params);
            memcpy(&temp, &message.params, sizeof(gQueuedCmdWriteIndex)) ;
            printf("temp:%d\r\n",temp);
#endif
            return &message.params;
            //printf("gQueuedCmdCurrentIndex:%d\r\n",gQueuedCmdCurrentIndex);
            //printf("gQueuedCmdWriteIndex:%d\r\n",gQueuedCmdWriteIndex);
        }
}



