#pragma once

#define ROBOT_AXIS                          (4)                         // 绂勭叅鑴濇鑴犺劊楣胯劥闄嗚劮鑴㈠獟


//-----------------------------------------------------------------------------
// Robot Profile mode
//-----------------------------------------------------------------------------
#define ROBOT_MODE_NONE                     (0)
#define ROBOT_MODE_SINGLE                   (1)
#define ROBOT_MODE_SINGLEXYZ                (2)
#define ROBOT_MODE_STOP                     (3)
#define ROBOT_MODE_HOME                     (4)
#define ROBOT_MODE_ARC                      (5)
#define ROBOT_MODE_PLAYBACK                 (6)
#define ROBOT_MODE_PLAYBACKWITHL            (7)
#define ROBOT_MODE_LEVELING                 (10)
#define ROBOT_MODE_CONTINUOUS_PATH          (16)

//#define ERROR_INV_CALCU                      (1)
//#define ERROR_INV_LIMIT                      (2)

#define LINE_MIN_DIST           0.0001f

#define LDEGREE                 0.1f
#define INP_MOTION_END          0x50
#define AXIS_SUM                5
//#define INTERP_RECI             (float)50
#define INTERP_RECI             (1/periodTime)
//纰岃尗闇茬倝鑴欓湶鎴劦鑴抽簱鑴ゅ崲state
enum {
    IDLE,
    AP_DOWN,
    AN_DOWN,
    BP_DOWN,
    BN_DOWN,
    CP_DOWN,
    CN_DOWN,
    DP_DOWN,
    DN_DOWN
};

enum {
    LP_DOWN = 9,
    LN_DOWN,
};

//鑴曢檰纰岃尗褰曠洸鑴劊闇茬倝鑴涙嫝鑴㈤檰type
enum {
    JUMP_XYZ,//鑴欒劀鑴ㄨ劌鑴劊闇茬倝
    MOVJ_XYZ,//楣胯劥闄嗚劮鑴劊闇茬倝
    MOVL_XYZ,//鑴板崵鑴ц劷鑴劊闇茬倝
    JUMP_ANGLE,
    MOVJ_ANGLE,
    MOVL_ANGLE,
    MOVJ_INC,
    MOVL_INC,
    MOVJ_XYZ_INC,
    JUMP_MOVL_XYZ,
};

enum {
    PRESEEK_HOME,
    SEEK_HOME,
    BACK_HOME,
    SEEK_Z,
    FINISH_HOME,
    SOFT_HOME,
    MOVE_NEXTPOINT,
    MOVE_NEXTTESTPOINT,
    BACK_FIRSTPOINT,
    BEGIN_FIRSTPOINT,
    DETECTION_RESULT,
    TEST_RESULT
};



