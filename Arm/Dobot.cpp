/****************************************Copyright(c)*****************************************************
**                            Shenzhen Yuejiang Technology Co., LTD.
**
**                                 http://www.dobot.cc
**
**--------------File Info---------------------------------------------------------------------------------
** File name:           Dobot.cpp
** Latest modified Date:2017-8-15
** Latest Version:      V1.0.0
** Descriptions:        Dobot body
**
**--------------------------------------------------------------------------------------------------------
** Created by:          Edward
** Created date:        2017-8-15
** Version:             V1.0.0
** Descriptions:        Mixly API
**--------------------------------------------------------------------------------------------------------
*********************************************************************************************************/
#include "command.h"
#include "type.h"
#include "stdio.h"
#include "Dobot.h"
#include "HardwareSerial.h"
/*
 *                Basic
 */
/*********************************************************************************************************
** Function name:       Dobot_GetDeviceTimeEX
** Descriptions:        Get Device Time
** Input parameters:    none
** Output parameters:   none
** Returned value:      none
*********************************************************************************************************/
uint32_t Dobot_GetDeviceTimeEx(void)
{
  //Struct Define
  uint32_t deviceTime;
  //Params

  //Send Command
  GetDeviceTime(&deviceTime,false,&gQueuedCmdIndex);
  
  //Wait Common Finish

  return deviceTime;
  }
/*********************************************************************************************************
** Function name:       Dobot_SetDeviceWIthLEX
** Descriptions:        Set L
** Input parameters:    none
** Output parameters:   none
** Returned value:      none
*********************************************************************************************************/
void Dobot_SetDeviceWIthLEx(bool isWithL)
{
  //Struct Define
  uint32_t deviceTime;
  //Params

  //Send Command
  SetDeviceWIthL(isWithL,false,&gQueuedCmdIndex);
  
  //Wait Common Finish

  }
/*********************************************************************************************************
** Function name:       Dobot_SetWAITCmdEx
** Descriptions:        Delay
** Input parameters:    wait time
** Output parameters:   none
** Returned value:      none
*********************************************************************************************************/
void Dobot_SetWAITCmdEx(float waittime){
  //Struct Define

  //Params

  //Send Command
  //delay(waittime);
  
  //Wait Common Finish
  //delay((uint32_t)waittime*1000);
  }

/*********************************************************************************************************
** Function name:       Dobot_GetPoseEx
** Descriptions:        Get Pose
** Input parameters:    none
** Output parameters:   none
** Returned value:      Pose
*********************************************************************************************************/
float Dobot_GetPoseEx(uint8_t temp)
{
  //Struct Define
  Pose pose;
  float poseL;
  //Params
  
  //Send Command
  GetPose(&pose,false,&gQueuedCmdIndex);
  
  //Wait Common Finish
  //delay((uint32_t)waittime*1000);
  //return Params
  switch(temp){
      case L:
            GetPoseL(&poseL,false,&gQueuedCmdIndex);
        return poseL;
      case X:
        return pose.x;
      break;
      case Y:
        return pose.y;
      break;
      case Z:
        return pose.z;
      break;
      case R:
        return pose.rHead;
      break;
      case JOINT1:
        return pose.jointAngle[0];
      break;
      case JOINT2:
        return pose.jointAngle[1];
      break;
      case JIONT3:
        return pose.jointAngle[2];
      break;
      case JIONT4:
        return pose.jointAngle[3];
      break;
      default:
        return 0;
      break;
    }
}
/*********************************************************************************************************
** Function name:       Dobot_SetHOMECmdEx
** Descriptions:        SetHome
** Input parameters:    none
** Output parameters:   none
** Returned value:      none
*********************************************************************************************************/
void Dobot_SetHOMECmdEx(void){
  //Struct Define

  //Params

  //Send Command
  SetHomeCmd(true,&gQueuedCmdIndex);
  
  //Wait Common Finish
    GetQueuedCmdCurrentIndex(false,&gQueuedCmdIndex);
    while(gQueuedCmdCurrentIndex != gQueuedCmdWriteIndex ){
        GetQueuedCmdCurrentIndex(false,&gQueuedCmdIndex);

    }
  }

/*********************************************************************************************************
** Function name:       
** Descriptions:        Set EndEffector Params
** Input parameters:    X,Y,Z
** Output parameters:   none
** Returned value:      none
*********************************************************************************************************/
void Dobot_SetEndEffectorParamsEx(float x,float y,float z) 
{
    //Struct Define
    EndEffectorParams endEffectorParams;
    
    //Params
    endEffectorParams.xBias= x;
    endEffectorParams.yBias = y;
    endEffectorParams.zBias = z;
    
    //Send Command
    SetEndEffectorParams(&endEffectorParams, true, &gQueuedCmdIndex);

    //Wait Common Finish
    
}

/*********************************************************************************************************
** Function name:       Dobot_SetEndEffectorLaserEx
** Descriptions:        Set Laser Statue
** Input parameters:    enable Duty
** Output parameters:   none
** Returned value:      none
*********************************************************************************************************/
void Dobot_SetEndEffectorLaserEx(uint8_t isEnable,float power)
{
    //Struct Define

    
    //Params

    //Send Command
    Dobot_SetIOMultiplexingEx(4,IOFunctionPWM);
    Dobot_SetIOPWMEx(4,40000,power);
    Dobot_SetIOMultiplexingEx(2,IOFunctionDO);
    Dobot_SetIODOEx(2,isEnable);

    //Wait Common Finish
}
/*********************************************************************************************************
** Function name:       Dobot_SetEndEffectorSuctionCupEx
** Descriptions:        Set SuctionCup
** Input parameters:    issuck
** Output parameters:   none
** Returned value:      none
*********************************************************************************************************/
void Dobot_SetEndEffectorSuctionCupEx(bool issuck) 
{
    //Struct Define
    
    //Params

    
    //Send Command
    SetEndEffectorSuctionCup(issuck, true, &gQueuedCmdIndex);

    //Wait Common Finish
    
}
/*********************************************************************************************************
** Function name:       Dobot_SeEndEffectorGritpperEx
** Descriptions:        Set Gripper
** Input parameters:    isEnable,isGriped
** Output parameters:   none
** Returned value:      none
*********************************************************************************************************/
void Dobot_SetEndEffectorGripperEx(bool isEnable,bool isGriped) 
{
    //Struct Define
    EndEffectorGripper endEffectorGripper;
    //Params
    endEffectorGripper.isEnable = isEnable;
    endEffectorGripper.isGriped = isGriped;
    
    //Send Command
    SeEndEffectorGritpper(&endEffectorGripper, true, &gQueuedCmdIndex);

    //Wait Common Finish
    
}


/*********************************************************************************************************
** Function name:       Dobot_SetPTPCommonParamsEx
** Descriptions:        Set PTPMommonParams
** Input parameters:    velocityRatio,accelerationRatio
** Output parameters:   none
** Returned value:      none
*********************************************************************************************************/
void Dobot_SetJOGCommonParamsEx(float velocityRatio,float accelerationRatio) 
{
    //Struct Define
    JOGCommonParams jogCommonParams;
    //Params
    jogCommonParams.velocityRatio = velocityRatio;
    jogCommonParams.accelerationRatio = accelerationRatio;
    
    //Send Command
    SetJOGCommonParams(&jogCommonParams, false, &gQueuedCmdIndex);

    //Wait Common Finish
}
/*********************************************************************************************************
** Function name:       Dobot_SetPTPJointParamsEx
** Descriptions:        Set PTP JointParams
** Input parameters:    Jointvelocity,Jointacceleration
** Output parameters:   none
** Returned value:      none
*********************************************************************************************************/
void Dobot_SetJOGJointParamsEx(float velocityJ1,float accelerationJ1,float velocityJ2,float accelerationJ2,float velocityJ3,float accelerationJ3,float velocityJ4,float accelerationJ4) 
{
    //Struct Define
    JOGJointParams jogJointParams;
    //Params
    jogJointParams.velocity[0] = velocityJ1;
    jogJointParams.acceleration[0] = accelerationJ1;
    jogJointParams.velocity[1] = velocityJ2;
    jogJointParams.acceleration[10] = accelerationJ2;
    jogJointParams.velocity[2] = velocityJ3;
    jogJointParams.acceleration[2] = accelerationJ3;
    jogJointParams.velocity[3] = velocityJ4;
    jogJointParams.acceleration[3] = accelerationJ4;
    
    //Send Command
    SetJOGJointParams(&jogJointParams, false, &gQueuedCmdIndex);

    //Wait Common Finish
}

/*********************************************************************************************************
** Function name:       Dobot_SetJOGCmdEx
** Descriptions:        Set JOGCmd
** Input parameters:    model
** Output parameters:   none
** Returned value:      none
*********************************************************************************************************/
void Dobot_SetJOGCmdEx(uint8_t model) 
{
    //Struct Define
    JOGCmd jogCmd;
    //Params
    if(model > 10){
        jogCmd.isJoint = true;
        jogCmd.cmd = model / 10;  
    }else{
        jogCmd.isJoint = false;
        jogCmd.cmd = model;  
    }
    
    //Send Command
    SetJOGCmd(&jogCmd, false, &gQueuedCmdIndex);

    //Wait Common Finish
}
/*********************************************************************************************************
** Function name:       Dobot_SetPTPCommonParamsEx
** Descriptions:        Set PTPMommonParams
** Input parameters:    velocityRatio,accelerationRatio
** Output parameters:   none
** Returned value:      none
*********************************************************************************************************/
void Dobot_SetPTPCommonParamsEx(float velocityRatio,float accelerationRatio) 
{
    //Struct Define
    PTPCommonParams ptpCommonParams;
    //Params
    ptpCommonParams.velocityRatio = velocityRatio;
    ptpCommonParams.accelerationRatio = accelerationRatio;
    
    //Send Command
    SetPTPCommonParams(&ptpCommonParams, false, &gQueuedCmdIndex);

    //Wait Common Finish
}
/*********************************************************************************************************
** Function name:       Dobot_SetPTPJointParamsEx
** Descriptions:        Set PTP JointParams
** Input parameters:    Jointvelocity,Jointacceleration
** Output parameters:   none
** Returned value:      none
*********************************************************************************************************/
void Dobot_SetPTPJointParamsEx(float velocityJ1,float accelerationJ1,float velocityJ2,float accelerationJ2,float velocityJ3,float accelerationJ3,float velocityJ4,float accelerationJ4) 
{
    //Struct Define
    PTPJointParams ptpJointParams;
    //Params
    ptpJointParams.velocity[0] = velocityJ1;
    ptpJointParams.acceleration[0] = accelerationJ1;
    ptpJointParams.velocity[1] = velocityJ2;
    ptpJointParams.acceleration[10] = accelerationJ2;
    ptpJointParams.velocity[2] = velocityJ3;
    ptpJointParams.acceleration[2] = accelerationJ3;
    ptpJointParams.velocity[3] = velocityJ4;
    ptpJointParams.acceleration[3] = accelerationJ4;
    
    //Send Command
    SetPTPJointParams(&ptpJointParams, false, &gQueuedCmdIndex);

    //Wait Common Finish
}
/*********************************************************************************************************
** Function name:       Dobot_SetPTPLParamsEx
** Descriptions:        Set L Params
** Input parameters:    velocityRatio,accelerationRatio
** Output parameters:   none
** Returned value:      none
*********************************************************************************************************/
void Dobot_SetPTPLParamsEx(float velocityRatio,float accelerationRatio) 
{
    //Struct Define
    PTPLParams ptpLParams;
    //Params
    ptpLParams.velocity = velocityRatio;
    ptpLParams.acceleration = accelerationRatio;
    
    //Send Command
    SetPTPLParams(&ptpLParams, false, &gQueuedCmdIndex);

    //Wait Common Finish
}
/*********************************************************************************************************
** Function name:       Dobot_SetPTPJumpParamsEx
** Descriptions:        Set Jump Height 
** Input parameters:    jumpHeight
** Output parameters:   none
** Returned value:      none
*********************************************************************************************************/
void Dobot_SetPTPJumpParamsEx(float jumpHeight) 
{
    //Struct Define
    PTPJumpParams ptpJumpParams;
    //Params
    ptpJumpParams.jumpHeight = jumpHeight;
    ptpJumpParams.zLimit = 100;
    
    //Send Command
    SetPTPJumpParams(&ptpJumpParams, false, &gQueuedCmdIndex);

    //Wait Common Finish
}
/*********************************************************************************************************
** Function name:       Dobot_SetPTPCmdEX
** Descriptions:        Wait For PTPMove 
** Input parameters:    Model,X,Y,Z,R
** Output parameters:   none
** Returned value:      none
*********************************************************************************************************/
void Dobot_SetPTPCmdEx(uint8_t Model,float x,float y,float z,float r) 
{
    //Struct Define
    PTPCmd ptpCmd;
    
    //Params
    ptpCmd.ptpMode = Model;
    ptpCmd.x = x;
    ptpCmd.y = y;
    ptpCmd.z = z;
    ptpCmd.rHead = r;

    //Send Command
    SetPTPCmd(&ptpCmd, true, &gQueuedCmdIndex);

    //Wait Common Finish
    GetQueuedCmdCurrentIndex(false,&gQueuedCmdIndex);
    while(gQueuedCmdCurrentIndex != gQueuedCmdWriteIndex ){
        GetQueuedCmdCurrentIndex(false,&gQueuedCmdIndex);
    }
}
/*********************************************************************************************************
** Function name:       Dobot_SetPTPCmdWithLEX
** Descriptions:        Wait For PTPMove 
** Input parameters:    Model,X,Y,Z,R,L
** Output parameters:   none
** Returned value:      none
*********************************************************************************************************/
void Dobot_SetPTPWithLCmdEx(uint8_t Model,float x,float y,float z,float r,float l) 
{
    //Struct Define
    PTPWithLCmd ptpWithLCmd;
    
    //Params
    ptpWithLCmd.ptpMode = Model;
    ptpWithLCmd.x = x;
    ptpWithLCmd.y = y;
    ptpWithLCmd.z = z;
    ptpWithLCmd.rHead = r;
    ptpWithLCmd.l = l;

    //Send Command
    SetPTPCmdWithL(&ptpWithLCmd, true, &gQueuedCmdIndex);

    //Wait Common Finish
    GetQueuedCmdCurrentIndex(false,&gQueuedCmdIndex);
    while(gQueuedCmdCurrentIndex != gQueuedCmdWriteIndex ){
        GetQueuedCmdCurrentIndex(false,&gQueuedCmdIndex);
    }
}
/*********************************************************************************************************
** Function name:       Dobot_SetIOMultiplexingEX
** Descriptions:        Set IO Config
** Input parameters:    address,function
** Output parameters:   none
** Returned value:      none
*********************************************************************************************************/
void Dobot_SetIOMultiplexingEx(uint8_t address,uint8_t function) 
{
    //Struct Define
    IOConfig iOConfig;
    
    //Params
    iOConfig.address = address;
    iOConfig.function = (IOFunction)function;
    
    //Send Command
    SetIOMultiplexing(&iOConfig, false, &gQueuedCmdIndex);

    //Wait Common Finish
}
/*********************************************************************************************************
** Function name:       Dobot_SetIODOEX
** Descriptions:        SetIOdo 
** Input parameters:    address,value
** Output parameters:   none
** Returned value:      none
*********************************************************************************************************/
void Dobot_SetIODOEx(uint8_t address,uint8_t value) 
{
    //Struct Define
    EIODO eIODO;
    
    //Params
    eIODO.address = address;
    eIODO.value = value;
    
    //Send Command
    SetIODO(&eIODO, false, &gQueuedCmdIndex);

    //Wait Common Finish
}
/*********************************************************************************************************
** Function name:       Dobot_SetIOPWMEX
** Descriptions:        SetIOPWM 
** Input parameters:    address,freq,duty
** Output parameters:   none
** Returned value:      none
*********************************************************************************************************/
void Dobot_SetIOPWMEx(uint8_t address,float freq,float duty) 
{
    //Struct Define
    EIOPWM eIOPWM;
    
    //Params
    eIOPWM.address = address;
    eIOPWM.freq = freq;
    eIOPWM.duty = duty;
    
    //Send Command
    SetIOPWM(&eIOPWM, false, &gQueuedCmdIndex);

    //Wait Common Finish
}
/*********************************************************************************************************
** Function name:       Dobot_GetIODIEX
** Descriptions:        GetIODI 
** Input parameters:    address
** Output parameters:   none
** Returned value:      value
*********************************************************************************************************/
uint8_t Dobot_GetIODIEx(uint8_t address) 
{
    //Struct Define
    EIODI eIODI;
    
    //Params
    eIODI.address = address;
    
    //Send Command
    GetIODI(&eIODI, false, &gQueuedCmdIndex);

    //Wait Common Finish

    return eIODI.value;
}
/*********************************************************************************************************
** Function name:       Dobot_GetIOADCEX
** Descriptions:        Get IOADC 
** Input parameters:    address
** Output parameters:   none
** Returned value:      adc
*********************************************************************************************************/
uint16_t Dobot_GetIOADCEx(uint8_t address) 
{
    //Struct Define
    EIOADC eIOADC;
    
    //Params
    eIOADC.address = address;
    
    //Send Command
    GetIOADC(&eIOADC, false, &gQueuedCmdIndex);

    //Wait Common Finish
    return eIOADC.adc;
}
/*********************************************************************************************************
** Function name:       Dobot_SetEMotorEX
** Descriptions:        Set EMotor 
** Input parameters:    address,enable,speed
** Output parameters:   none
** Returned value:      none
*********************************************************************************************************/
void Dobot_SetEMotorEx(uint8_t address,uint8_t enable,int32_t speed) 
{
    //Struct Define
    EMotor eMotor;
    
    //Params
    eMotor.address = address;
    eMotor.enable = enable;
    eMotor.speed = speed;
    
    //Send Command
    SetEMotor(&eMotor, false, &gQueuedCmdIndex);

    //Wait Common Finish
}
/*********************************************************************************************************
** Function name:       Dobot_SetEMotorSEX
** Descriptions:        Set EMotor 
** Input parameters:    address,enable,speed,deltaPluse
** Output parameters:   none
** Returned value:      none
*********************************************************************************************************/
void Dobot_SetEMotorSEx(uint8_t address,uint8_t enable,int32_t speed,uint32_t deltaPulse) 
{
    //Struct Define
    EMotorS eMotorS;
    
    //Params
    eMotorS.address = address;
    eMotorS.enable = enable;
    eMotorS.speed = speed;
    eMotorS.deltaPulse = deltaPulse;
    
    //Send Command
    SetEMotorS(&eMotorS, false, &gQueuedCmdIndex);

    //Wait Common Finish
}
/*********************************************************************************************************
** Function name:       Dobot_SetColorSensorEX
** Descriptions:        Set ColorSensor
** Input parameters:    enable,port
** Output parameters:   none
** Returned value:      none
*********************************************************************************************************/
void Dobot_SetColorSensorEx(uint8_t enable,uint8_t port) 
{
    //Struct Define
    ColorSensor colorSensor;
    
    //Params
    colorSensor.enable = enable;
    colorSensor.port = port;
    
    //Send Command
    SetColorSensor(&colorSensor, false, &gQueuedCmdIndex);

    //Wait Common Finish
}
/*********************************************************************************************************
** Function name:       Dobot_GetColorSensorEX
** Descriptions:        Set Color
** Input parameters:    color
** Output parameters:   none
** Returned value:      none
*********************************************************************************************************/
uint8_t Dobot_GetColorSensorEx(uint8_t color) 
{
    //Struct Define
    ColorSensor colorSensor;
    
    //Params

    
    //Send Command
    GetColorSensor(&colorSensor, false, &gQueuedCmdIndex);

    //Wait Common Finish
    switch(color){
    case 0:
        return colorSensor.r;
    break;  
    case 1:
        return colorSensor.g;
    break;  
    case 2:
        return colorSensor.b;
    break;   
    default:
    break; 
    }
}
/*********************************************************************************************************
** Function name:       Dobot_SetIRSwitchEX
** Descriptions:        Set IR Switch
** Input parameters:    enable,port
** Output parameters:   none
** Returned value:      none
*********************************************************************************************************/
void Dobot_SetIRSwitchEx(uint8_t enable,uint8_t port) 
{
    //Struct Define
    IRSwitch iRSwitch;
    
    //Params
    iRSwitch.enable = enable;
    iRSwitch.port = port;
    
    //Send Command
    SetIRSwitch(&iRSwitch, false, &gQueuedCmdIndex);

    //Wait Common Finish
}
/*********************************************************************************************************
** Function name:       GetIRSwitchEX
** Descriptions:        Get IR Switch
** Input parameters:    port
** Output parameters:   none
** Returned value:      value
*********************************************************************************************************/
uint8_t GetIRSwitchEx(uint8_t port) 
{
    //Struct Define
    IRSwitch iRSwitch;
    
    //Params
    iRSwitch.port = port;
    
    //Send Command
    GetIRSwitch(&iRSwitch, false, &gQueuedCmdIndex);

    //Wait Common Finish

    return iRSwitch.value;

}
/*********************************************************************************************************
** Function name:       Dobot_SetLostStepSetValueEX
** Descriptions:        SetLostStepSetValue
** Input parameters:    value
** Output parameters:   none
** Returned value:      none
*********************************************************************************************************/
void Dobot_SetLostStepSetEx(float lostStepValue) 
{
    //Struct Define
    SysParams sysParams;
    
    //Params
    sysParams.lostStepValue = lostStepValue;
    
    //Send Command
    SetLostStepSetValue(&sysParams, false, &gQueuedCmdIndex);

    //Wait Common Finish
}
/*********************************************************************************************************
** Function name:       Dobot_SetLostStepDetectEX
** Descriptions:        SetLostStepDetect 
** Input parameters:    none
** Output parameters:   none
** Returned value:      none
*********************************************************************************************************/
void Dobot_SetLostStepCmdEx(void) 
{
    //Struct Define
    
    //Params
    
    //Send Command
    SetLostStepDetect(false, &gQueuedCmdIndex);

    //Wait Common Finish
    
}



void Dobot_SetMotorPulseEx(float joint0 , float joint1 , float joint2 , float joint3 , float joint_re1 , float joint_re2)
{
    //Struct Define
    PulseCmd pulseCmd;
    
    //Params
    pulseCmd.params[0] = joint0;
    pulseCmd.params[1] = joint1;
    pulseCmd.params[2] = joint2;
    pulseCmd.params[3] = joint3;
    pulseCmd.params[4] = joint_re1;
    pulseCmd.params[5] = joint_re2;

    //Send Command
    SetMotorPulse(&pulseCmd, true, &gQueuedCmdIndex);

    //Wait Common Finish
    GetQueuedCmdCurrentIndex(false,&gQueuedCmdIndex);
    printf("gQueuedCmdCurrentIndex:%d\r\n",gQueuedCmdCurrentIndex);
    while(gQueuedCmdCurrentIndex != gQueuedCmdWriteIndex ){
      printf("gQueuedCmdWriteIndex:%d\r\n",gQueuedCmdWriteIndex);
        GetQueuedCmdCurrentIndex(false,&gQueuedCmdIndex);
    }
  
  
}



