/****************************************Copyright(c)*****************************************************
**                            Shenzhen Yuejiang Technology Co., LTD.
**
**                                 http://www.dobot.cc
**
**--------------File Info---------------------------------------------------------------------------------
** File name:           Dobot.h
** Latest modified Date:2017-8-15
** Latest Version:      V1.0.0
** Descriptions:        
**
**--------------------------------------------------------------------------------------------------------
** Created by:          Edward
** Created date:        2017-8-15
** Version:             V1.0.0
** Descriptions:        Mixly API
**--------------------------------------------------------------------------------------------------------
*********************************************************************************************************/
#include "type.h"
#include "Command.h"
#include "stdio.h"
#define SERIAL_TX_BUFFER_SIZE 64
#define SERIAL_RX_BUFFER_SIZE 256
typedef enum tagPos{
    L,
    X,
    Y,
    Z,
    R,
    JOINT1,
    JOINT2,
    JIONT3,
    JIONT4
}Pos;

typedef enum tagIOFunction {
    IOFunctionDummy,
    IOFunctionDO,
    IOFunctionPWM,
    IOFunctionDI,
    IOFunctionADC,
    IOFunctionDIPU,
    IOFunctionDIPD,
} IOFunction;

extern uint64_t gQueuedCmdIndex;
extern uint64_t gQueuedCmdWriteIndex;
extern uint64_t gQueuedCmdCurrentIndex;
extern uint8_t gQueueCmdLeftSpace;
/*********************************************************************************************************
** Device Init
*********************************************************************************************************/
extern void Dobot_Init();
/*********************************************************************************************************
** Device function
*********************************************************************************************************/
extern uint32_t Dobot_GetDeviceTimeEx(void);
extern void Dobot_SetDeviceWIthLEx(bool isWithL);
/*********************************************************************************************************
** Home function
*********************************************************************************************************/
extern void Dobot_SetHOMECmdEx(void);
extern float Dobot_GetPoseEx(uint8_t temp);
/*********************************************************************************************************
** EndEffector function
*********************************************************************************************************/
extern void Dobot_SetEndEffectorParamsEx(float x,float y,float z) ;
extern void Dobot_SetEndEffectorLaserEx(uint8_t isEnable,float power) ;
extern void Dobot_SetEndEffectorSuctionCupEx(bool issuck) ;
extern void Dobot_SetEndEffectorGripperEx(bool isEnable,bool isGriped) ;
/*********************************************************************************************************
** JOG function
*********************************************************************************************************/
extern void Dobot_SetJOGCommonParamsEx(float velocityRatio,float accelerationRatio) ;
extern void Dobot_SetJOGJointParamsEx(float velocityJ1,float accelerationJ1,float velocityJ2,float accelerationJ2,float velocityJ3,float accelerationJ3,float velocityJ4,float accelerationJ4) ;
extern void Dobot_SetJOGCmdEx(uint8_t model) ;
/*********************************************************************************************************
** PTP function
*********************************************************************************************************/
extern void Dobot_SetPTPCommonParamsEx(float velocityRatio,float accelerationRatio) ;
extern void Dobot_SetPTPJointParamsEx(float velocityJ1,float accelerationJ1,float velocityJ2,float accelerationJ2,float velocityJ3,float accelerationJ3,float velocityJ4,float accelerationJ4) ;
extern void Dobot_SetPTPLParamsEx(float velocityRatio,float accelerationRatio) ;
extern void Dobot_SetPTPJumpParamsEx(float jumpHeight) ;
extern void Dobot_SetPTPCmdEx(uint8_t Model,float x,float y,float z,float r) ;
extern void Dobot_SetPTPWithLCmdEx(uint8_t Model,float x,float y,float z,float r,float l) ;

/*********************************************************************************************************
** EIO function
*********************************************************************************************************/
extern void Dobot_SetIOMultiplexingEx(uint8_t address,uint8_t function) ;
extern void Dobot_SetIODOEx(uint8_t address,uint8_t value) ;
extern void Dobot_SetIOPWMEx(uint8_t address,float freq,float duty);
extern uint8_t Dobot_GetIODIEx(uint8_t address) ;
extern uint16_t Dobot_GetIOADCEx(uint8_t address) ;
extern void Dobot_SetEMotorEx(uint8_t address,uint8_t enable,int32_t speed) ;
extern void Dobot_SetEMotorSEx(uint8_t address,uint8_t enable,int32_t speed,uint32_t deltaPulse) ;
extern void Dobot_SetColorSensorEx(uint8_t enable,uint8_t port) ;
extern uint8_t Dobot_GetColorSensorEx(uint8_t color) ;
extern void Dobot_SetIRSwitchEx(uint8_t enable,uint8_t port) ;
extern uint8_t GetIRSwitchEx(uint8_t port) ;
/*********************************************************************************************************
** LostStep function
*********************************************************************************************************/
extern void Dobot_SetLostStepSetEx(float lostStepValue) ;
extern void Dobot_SetLostStepCmdEx(void) ;
//temp test
extern void Dobot_SetMotorPulseEx(float joint0 , float joint1 , float joint2 , float joint3 , float joint_re1 , float joint_re2);



