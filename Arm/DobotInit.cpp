/****************************************Copyright(c)*****************************************************
**                            Shenzhen Yuejiang Technology Co., LTD.
**
**                                 http://www.dobot.cc
**
**--------------File Info---------------------------------------------------------------------------------
** File name:           DobotInit.cpp
** Latest modified Date:2017-8-15
** Latest Version:      V1.0.0
** Descriptions:        Init body
**
**--------------------------------------------------------------------------------------------------------
** Created by:          Edward
** Created date:        2017-8-15
** Version:             V1.0.0
** Descriptions:        Init Dobot
**--------------------------------------------------------------------------------------------------------
*********************************************************************************************************/

#include "FlexiTimer2.h"
#include "stdio.h"
#include "HardwareSerial.h"
#include "Protocol.h"
#include "Dobot.h"
//#include "command.h"

//Set Serial TX&RX Buffer Size

#define SerialNum Serial3                                                                   //娴ｈ法鏁ゆ稉鎻掑經3
uint64_t gQueuedCmdIndex;
uint64_t gQueuedCmdWriteIndex;
uint64_t gQueuedCmdCurrentIndex;
uint8_t gQueueCmdLeftSpace;
uint32_t gSystick;
/*********************************************************************************************************

                                         Serial Config

*********************************************************************************************************/
/*********************************************************************************************************
** Function name:       Serialread
** Descriptions:        import data to rxbuffer
** Input parametersnone:
** Output parameters:   
** Returned value:      
*********************************************************************************************************/
void Serialread()
{
    while(SerialNum.available()) {
        uint8_t data = SerialNum.read();
        if (RingBufferIsFull(&gSerialProtocolHandler.rxRawByteQueue) == false) {
            RingBufferEnqueue(&gSerialProtocolHandler.rxRawByteQueue, &data);
        }
    }
}
/*********************************************************************************************************
** Function name:       Serial_putc
** Descriptions:        Remap Serial to Printf
** Input parametersnone:
** Output parameters:   
** Returned value:       
*********************************************************************************************************/
int Serial_putc( char c, struct __file * )
{
    Serial.write( c );
    return c;
}

/*********************************************************************************************************
** Function name:       printf_begin
** Descriptions:        Initializes Printf
** Input parameters:    
** Output parameters:
** Returned value:      
*********************************************************************************************************/
void printf_begin(void)
{
    fdevopen( &Serial_putc, 0 );
}
/*********************************************************************************************************
** Function name:       Dobot_Init
** Descriptions:        Init Dobot
** Input parameters:    
** Output parameters:
** Returned value:      
*********************************************************************************************************/
void Dobot_Init() {
    Serial.begin(115200);
    SerialNum.begin(115200); 
    
    //Enable Printf
    printf_begin();
    
    //Set Timer IRQ閿涘湩ead SerialNum Buffer閿涳拷 
    FlexiTimer2::set(100,Serialread); 
    FlexiTimer2::start();
    
    ProtocolInit();
    //reset All IO
    for(int i = 0;i<21;i++){
        Dobot_SetIOMultiplexingEx(i,IOFunctionDI);
    }
    SetQueuedCmdStartExec(true,&gQueuedCmdIndex);
}



