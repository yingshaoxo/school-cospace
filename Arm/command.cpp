/****************************************Copyright(c)*****************************************************
**                            Shenzhen Yuejiang Technology Co., LTD.
**
**                                 http://www.dobot.cc
**
**--------------File Info---------------------------------------------------------------------------------
** File name:           Command.cpp
** Latest modified Date:2017-8-15
** Latest Version:      V1.0.0
** Descriptions:        Command body
**
**--------------------------------------------------------------------------------------------------------
** Created by:          Edward
** Created date:        2017-8-15
** Version:             V1.0.0
** Descriptions:        Command API
**--------------------------------------------------------------------------------------------------------
*********************************************************************************************************/

#include <stdio.h>
#include <string.h>
#include <arduino.h>
#include "command.h"
#include "Protocol.h"
#include "ProtocolID.h"
#include "type.h"

bool  IsCmdCallBack[ProtocolMax];
extern bool  gQueuedCmdFlag;
extern uint64_t gQueuedCmdIndex;
extern uint64_t gQueuedCmdWriteIndex;
extern uint64_t gQueuedCmdCurrentIndex;
extern uint8_t gQueueCmdLeftSpace;
/*********************************************************************************************************
** Function name:       SetDeviceWIthL
** Descriptions:        Set end effector parameters
** Input parameters:    endEffectorParams, isQueued
** Output parameters:   queuedCmdIndex
** Returned value:      true
*********************************************************************************************************/
int SetDeviceWIthL(bool isWithL,bool isQueued, uint64_t *queuedCmdIndex)
{
    Message tempMessage;

    memset(&tempMessage, 0, sizeof(Message));
    tempMessage.id = ProtocolDeviceWithL;
    tempMessage.rw = true;
    tempMessage.isQueued = isQueued;
    tempMessage.paramsLen = sizeof(isWithL);
    tempMessage.params[0] = isWithL;

    IsCmdCallBack[tempMessage.id] = false;
    
    MessageWrite(&gSerialProtocolHandler, &tempMessage);
    //wait command Execution
    uint32_t temppointer;
    while (IsCmdCallBack[tempMessage.id] == false ) {
        temppointer = ProtocolProcess();
    }

    //copy params receive
    //memcpy(&gDeviceID,temppointer,3*sizeof(uint32_t));

    //(*queuedCmdIndex)++;
    return true;
}
/*********************************************************************************************************
** Function name:       GetDeviceID
** Descriptions:        Set end effector parameters
** Input parameters:    endEffectorParams, isQueued
** Output parameters:   queuedCmdIndex
** Returned value:      true
*********************************************************************************************************/

int GetDeviceID(uint32_t *devicetime,bool isQueued, uint64_t *queuedCmdIndex)
{
    Message tempMessage;

    memset(&tempMessage, 0, sizeof(Message));
    tempMessage.id = ProtocolDeviceID;
    tempMessage.rw = false;
    tempMessage.isQueued = isQueued;
    gQueuedCmdFlag = isQueued;
    tempMessage.paramsLen = 0;
    //memcpy(tempMessage.params, (uint8_t *)endEffectorParams, tempMessage.paramsLen);

    IsCmdCallBack[tempMessage.id] = false;
    
    MessageWrite(&gSerialProtocolHandler, &tempMessage);
    //wait command Execution
    uint32_t temppointer;
    while (IsCmdCallBack[tempMessage.id] == false ) {
        temppointer = ProtocolProcess();
    }

    //copy params receive
    memcpy(devicetime,temppointer,sizeof(uint32_t));

    //(*queuedCmdIndex)++;
    return true;
}
/*********************************************************************************************************
** Function name:       GetDeviceTime
** Descriptions:        Get DeviceTime
** Input parameters:    deviceTime, isQueued
** Output parameters:   queuedCmdIndex
** Returned value:      true
*********************************************************************************************************/
int GetDeviceTime(uint32_t *deviceTime,bool isQueued, uint64_t *queuedCmdIndex)
{
    Message tempMessage;

    memset(&tempMessage, 0, sizeof(Message));
    tempMessage.id = ProtocolDeviceTime;
    tempMessage.rw = false;
    tempMessage.isQueued = isQueued;
    tempMessage.paramsLen = 0;
    //memcpy(tempMessage.params, (uint8_t *)endEffectorParams, tempMessage.paramsLen);

    IsCmdCallBack[tempMessage.id] = false;
    
    MessageWrite(&gSerialProtocolHandler, &tempMessage);
    //wait command Execution
    uint32_t temppointer;
    while (IsCmdCallBack[tempMessage.id] == false ) {
        temppointer = ProtocolProcess();
    }
    //copy params receive
    if(isQueued){
          memcpy(&gQueuedCmdWriteIndex,temppointer,sizeof(gQueuedCmdWriteIndex));
          memcpy(deviceTime,temppointer-sizeof(gQueuedCmdWriteIndex),sizeof(uint32_t));
      }else{
          memcpy(deviceTime,temppointer,sizeof(uint32_t));        
      }
    
    return true;
}

/*********************************************************************************************************
** Function name:       GetPose
** Descriptions:        Set end effector parameters
** Input parameters:    endEffectorParams, isQueued
** Output parameters:   queuedCmdIndex
** Returned value:      true
*********************************************************************************************************/

int GetPose(Pose *pose,bool isQueued, uint64_t *queuedCmdIndex)
{
    Message tempMessage;

    memset(&tempMessage, 0, sizeof(Message));
    tempMessage.id = ProtocolGetPose;
    tempMessage.rw = false;
    tempMessage.isQueued = isQueued;
    tempMessage.paramsLen = 0;
    //memcpy(tempMessage.params, (uint8_t *)endEffectorParams, tempMessage.paramsLen);

    IsCmdCallBack[tempMessage.id] = false;
    
    MessageWrite(&gSerialProtocolHandler, &tempMessage);
    //wait command Execution
    uint32_t temppointer;
    while (IsCmdCallBack[tempMessage.id] == false ) {
        temppointer = ProtocolProcess();
    }
    //copy params receive
    memcpy(pose,temppointer,sizeof(Pose));
    //(*queuedCmdIndex)++;
    return true;
}
/*********************************************************************************************************
** Function name:       GetPose
** Descriptions:        Set end effector parameters
** Input parameters:    endEffectorParams, isQueued
** Output parameters:   queuedCmdIndex
** Returned value:      true
*********************************************************************************************************/

int GetPoseL(float *poseL,bool isQueued, uint64_t *queuedCmdIndex)
{
    Message tempMessage;

    memset(&tempMessage, 0, sizeof(Message));
    tempMessage.id = ProtocolGetPoseL;
    tempMessage.rw = false;
    tempMessage.isQueued = isQueued;
    tempMessage.paramsLen = 0;
    //memcpy(tempMessage.params, (uint8_t *)endEffectorParams, tempMessage.paramsLen);

    IsCmdCallBack[tempMessage.id] = false;
    
    MessageWrite(&gSerialProtocolHandler, &tempMessage);
    //wait command Execution
    uint32_t temppointer;
    while (IsCmdCallBack[tempMessage.id] == false ) {
        temppointer = ProtocolProcess();
    }
    //copy params receive
    memcpy(poseL,temppointer,sizeof(float));
    //(*queuedCmdIndex)++;
    return true;
}
/*********************************************************************************************************
** Function name:       SetHomeCmd
** Descriptions:        Set Hmoe 
** Input parameters:     isQueued
** Output parameters:   queuedCmdIndex
** Returned value:      true
*********************************************************************************************************/
int SetHomeCmd(bool isQueued, uint64_t *queuedCmdIndex)
{
    Message tempMessage;

    memset(&tempMessage, 0, sizeof(Message));
    tempMessage.id = ProtocolHOMECmd;
    tempMessage.rw = true;
    tempMessage.isQueued = isQueued;
    tempMessage.paramsLen = 0;
    //memcpy(tempMessage.params, (uint8_t *)endEffectorParams, tempMessage.paramsLen);

    IsCmdCallBack[tempMessage.id] = false;
    
    MessageWrite(&gSerialProtocolHandler, &tempMessage);
    //wait command Execution
    uint32_t temppointer;
    while (IsCmdCallBack[tempMessage.id] == false ) {
        temppointer = ProtocolProcess();
    }
    //copy params receive
    memcpy(&gQueuedCmdWriteIndex,temppointer,sizeof(uint64_t));
    return true;
}
/*********************************************************************************************************
** Function name:       SetHomeCmd
** Descriptions:        Set Hmoe 
** Input parameters:     isQueued
** Output parameters:   queuedCmdIndex
** Returned value:      true
*********************************************************************************************************/
int SetHomeParamsCmd(Pose *pose,bool isQueued, uint64_t *queuedCmdIndex)
{
    Message tempMessage;


    memset(&tempMessage, 0, sizeof(Message));
    tempMessage.id = ProtocolHOMEParams;
    tempMessage.rw = true;
    tempMessage.isQueued = isQueued;
    tempMessage.paramsLen = 0;
    memcpy(tempMessage.params, (uint8_t *)pose, tempMessage.paramsLen);

    IsCmdCallBack[tempMessage.id] = false;
    
    MessageWrite(&gSerialProtocolHandler, &tempMessage);
    //wait command Execution
    uint32_t temppointer;
    while (IsCmdCallBack[tempMessage.id] == false ) {
        temppointer = ProtocolProcess();
    }
    //copy params receive
    //memcpy(&gQueuedCmdWriteIndex,temppointer,sizeof(uint64_t));
    return true;
}
/*********************************************************************************************************
** Function name:       SetHomeCmd
** Descriptions:        Set Hmoe 
** Input parameters:     isQueued
** Output parameters:   queuedCmdIndex
** Returned value:      true
*********************************************************************************************************/
uint32_t GetHomeParamsCmd(bool isQueued, uint64_t *queuedCmdIndex)
{
    Message tempMessage;

    memset(&tempMessage, 0, sizeof(Message));
    tempMessage.id = ProtocolHOMEParams;
    tempMessage.rw = false;
    tempMessage.isQueued = isQueued;
    tempMessage.paramsLen = 0;
    //memcpy(tempMessage.params, (uint8_t *)endEffectorParams, tempMessage.paramsLen);

    IsCmdCallBack[tempMessage.id] = false;
    
    MessageWrite(&gSerialProtocolHandler, &tempMessage);
    //wait command Execution
    uint32_t temppointer;
    while (IsCmdCallBack[tempMessage.id] == false ) {
        temppointer = ProtocolProcess();
    }
    //copy params receive
    //memcpy(&gQueuedCmdWriteIndex,temppointer,sizeof(uint64_t));
    return temppointer;
}
/*********************************************************************************************************
** Function name:       SetEndEffectorParams
** Descriptions:        Set end effector parameters
** Input parameters:    endEffectorParams, isQueued
** Output parameters:   queuedCmdIndex
** Returned value:      true
*********************************************************************************************************/
int SetEndEffectorParams(EndEffectorParams *endEffectorParams, bool isQueued, uint64_t *queuedCmdIndex)
{
    Message tempMessage;

    memset(&tempMessage, 0, sizeof(Message));
    tempMessage.id = ProtocolEndEffectorParams;
    tempMessage.rw = true;
    tempMessage.isQueued = isQueued;
    tempMessage.paramsLen = sizeof(EndEffectorParams);
    memcpy(tempMessage.params, (uint8_t *)endEffectorParams, tempMessage.paramsLen);

    IsCmdCallBack[tempMessage.id] = false;
    
    MessageWrite(&gSerialProtocolHandler, &tempMessage);
    //wait command Execution
    while (IsCmdCallBack[tempMessage.id] == false ) {
        ProtocolProcess();
    }

    //(*queuedCmdIndex)++;
    return true;
}

/*********************************************************************************************************
** Function name:       SetEndEffectorLaser
** Descriptions:        Set the laser output
** Input parameters:    on,isQueued
** Output parameters:   queuedCmdIndex
** Returned value:      true
*********************************************************************************************************/
int SetEndEffectorLaser(bool ison, bool isQueued, uint64_t *queuedCmdIndex)
{
    Message tempMessage;

    memset(&tempMessage, 0, sizeof(Message));
    tempMessage.id = ProtocolEndEffectorLaser;
    tempMessage.rw = true;
    tempMessage.isQueued = isQueued;
    tempMessage.paramsLen = sizeof(ison);
    tempMessage.params[0] = ison;

    IsCmdCallBack[tempMessage.id] = false;
    
    MessageWrite(&gSerialProtocolHandler, &tempMessage);
    //wait command Execution
    while (IsCmdCallBack[tempMessage.id] == false ) {
        ProtocolProcess();
    }
    //(*queuedCmdIndex)++;
    return true;
}

/*********************************************************************************************************
** Function name:       SetEndEffectorSuctionCup
** Descriptions:        Set the suctioncup output
** Input parameters:    suck,isQueued
** Output parameters:   queuedCmdIndex
** Returned value:      true
*********************************************************************************************************/
int SetEndEffectorSuctionCup(bool issuck, bool isQueued, uint64_t *queuedCmdIndex)
{
    Message tempMessage;

    memset(&tempMessage, 0, sizeof(Message));
    tempMessage.id = ProtocolEndEffectorSuctionCup;
    tempMessage.rw = true;
    tempMessage.isQueued = isQueued;
    tempMessage.paramsLen = 2;
    tempMessage.params[0] = issuck;
    tempMessage.params[1] = issuck;
    
	  IsCmdCallBack[tempMessage.id] = false;
    
    MessageWrite(&gSerialProtocolHandler, &tempMessage);
    //wait command Execution
    while (IsCmdCallBack[tempMessage.id] == false ) {
        ProtocolProcess();
    }
    //(*queuedCmdIndex)++;
    return true;
}

/*********************************************************************************************************
** Function name:       SetEndEffectorGripper
** Descriptions:        Set the gripper output
** Input parameters:    grip,isQueued
** Output parameters:   queuedCmdIndex
** Returned value:      true
*********************************************************************************************************/
int SeEndEffectorGritpper(EndEffectorGripper *endEffectorGripper, bool isQueued, uint64_t *queuedCmdIndex)
{
    Message tempMessage;

    memset(&tempMessage, 0, sizeof(Message));
    tempMessage.id = ProtocolEndEffectorGripper;
    tempMessage.rw = true;
    tempMessage.isQueued = isQueued;
    tempMessage.paramsLen = sizeof(EndEffectorGripper);
    tempMessage.params[0] = endEffectorGripper->isEnable;
    tempMessage.params[1] = endEffectorGripper->isGriped;
    
	IsCmdCallBack[tempMessage.id] = false;
    
    MessageWrite(&gSerialProtocolHandler, &tempMessage);
    //wait command Execution
    while (IsCmdCallBack[tempMessage.id] == false ) {
        ProtocolProcess();
    }
    //(*queuedCmdIndex)++;
    return true;
}

/*********************************************************************************************************
** Function name:       SetJOGJointParams
** Descriptions:        Sets the joint jog parameter
** Input parameters:    jogJointParams,isQueued
** Output parameters:   queuedCmdIndex
** Returned value:      true
*********************************************************************************************************/
int SetJOGJointParams(JOGJointParams *jogJointParams, bool isQueued, uint64_t *queuedCmdIndex)
{
    Message tempMessage;

    memset(&tempMessage, 0, sizeof(Message));
    tempMessage.id = ProtocolJOGJointParams;
    tempMessage.rw = true;
    tempMessage.isQueued = isQueued;
    tempMessage.paramsLen = sizeof(JOGJointParams);
    memcpy(tempMessage.params, (uint8_t *)jogJointParams, tempMessage.paramsLen);

    IsCmdCallBack[tempMessage.id] = false;
    
    MessageWrite(&gSerialProtocolHandler, &tempMessage);
    //wait command Execution
    while (IsCmdCallBack[tempMessage.id] == false ) {
        ProtocolProcess();
    }
    //(*queuedCmdIndex)++;
    return true;
}

/*********************************************************************************************************
** Function name:       SetJOGCoordinateParams
** Descriptions:        Sets the axis jog parameter
** Input parameters:    jogCoordinateParams,isQueued
** Output parameters:   queuedCmdIndex
** Returned value:      true
*********************************************************************************************************/
int SetJOGCoordinateParams(JOGCoordinateParams *jogCoordinateParams, bool isQueued, uint64_t *queuedCmdIndex)
{
    Message tempMessage;

    memset(&tempMessage, 0, sizeof(Message));
    tempMessage.id = ProtocolJOGCoordinateParams;
    tempMessage.rw = true;
    tempMessage.isQueued = isQueued;
    tempMessage.paramsLen = sizeof(JOGCoordinateParams);
    memcpy(tempMessage.params, (uint8_t *)jogCoordinateParams, tempMessage.paramsLen);

    IsCmdCallBack[tempMessage.id] = false;
    
    MessageWrite(&gSerialProtocolHandler, &tempMessage);
    //wait command Execution
    while (IsCmdCallBack[tempMessage.id] == false ) {
        ProtocolProcess();
    }
    //(*queuedCmdIndex)++;
    return true;
}

/*********************************************************************************************************
** Function name:       SetJOGCommonParams
** Descriptions:        Sets the jog common parameter
** Input parameters:    jogCommonParams,isQueued
** Output parameters:   queuedCmdIndex
** Returned value:      true
*********************************************************************************************************/
int SetJOGCommonParams(JOGCommonParams *jogCommonParams, bool isQueued, uint64_t *queuedCmdIndex)
{
    Message tempMessage;

    memset(&tempMessage, 0, sizeof(Message));
    tempMessage.id = ProtocolJOGCommonParams;
    tempMessage.rw = true;
    tempMessage.isQueued = isQueued;
    tempMessage.paramsLen = sizeof(JOGCommonParams);
    memcpy(tempMessage.params, (uint8_t *)jogCommonParams, tempMessage.paramsLen);

    IsCmdCallBack[tempMessage.id] = false;
    
    MessageWrite(&gSerialProtocolHandler, &tempMessage);
    //wait command Execution
    while (IsCmdCallBack[tempMessage.id] == false ) {
        ProtocolProcess();
    }
    //(*queuedCmdIndex)++;
    return true;
}

/*********************************************************************************************************
** Function name:       SetJOGCmd
** Descriptions:        Execute the jog function
** Input parameters:    jogCmd,isQueued
** Output parameters:   queuedCmdIndex
** Returned value:      true
*********************************************************************************************************/
int SetJOGCmd(JOGCmd *jogCmd, bool isQueued, uint64_t *queuedCmdIndex)
{
    Message tempMessage;

    memset(&tempMessage, 0, sizeof(Message));
    tempMessage.id = ProtocolJOGCmd;
    tempMessage.rw = true;
    tempMessage.isQueued = isQueued;
    tempMessage.paramsLen = sizeof(JOGCmd);
    memcpy(tempMessage.params, (uint8_t *)jogCmd, tempMessage.paramsLen);

    IsCmdCallBack[tempMessage.id] = false;
    
    MessageWrite(&gSerialProtocolHandler, &tempMessage);
    //wait command Execution
    while (IsCmdCallBack[tempMessage.id] == false ) {
        ProtocolProcess();
    }
    //(*queuedCmdIndex)++;
    return true;
}

/*********************************************************************************************************
** Function name:       SetPTPJointParams
** Descriptions:        Sets the articulation point parameter
** Input parameters:    ptpJointParams,isQueued
** Output parameters:   queuedCmdIndex
** Returned value:      true
*********************************************************************************************************/
int SetPTPJointParams(PTPJointParams *ptpJointParams, bool isQueued, uint64_t *queuedCmdIndex)
{
    Message tempMessage;
    memset(&tempMessage, 0, sizeof(Message));
    tempMessage.id = ProtocolPTPJointParams;
    tempMessage.rw = true;
    tempMessage.isQueued = isQueued;
    tempMessage.paramsLen = sizeof(PTPJointParams);
    memcpy(tempMessage.params,(uint8_t *)ptpJointParams, tempMessage.paramsLen);

    IsCmdCallBack[tempMessage.id] = false;
    
    MessageWrite(&gSerialProtocolHandler, &tempMessage);
    //wait command Execution
    while (IsCmdCallBack[tempMessage.id] == false ) {
        ProtocolProcess();
    }
    //(*queuedCmdIndex)++;
    return true;
}

/*********************************************************************************************************
** Function name:       SetPTPCoordinateParams
** Descriptions:        Sets the coordinate position parameter
** Input parameters:    ptpCoordinateParams,isQueued
** Output parameters:   queuedCmdIndex
** Returned value:      true
*********************************************************************************************************/
int SetPTPCoordinateParams(PTPCoordinateParams *ptpCoordinateParams, bool isQueued, uint64_t *queuedCmdIndex)
{
    Message tempMessage;
    
    memset(&tempMessage, 0, sizeof(Message));
    tempMessage.id = ProtocolPTPCoordinateParams;
    tempMessage.rw = true;
    tempMessage.isQueued = isQueued;
    tempMessage.paramsLen = sizeof(PTPCoordinateParams);
    memcpy(tempMessage.params, (uint8_t *)ptpCoordinateParams, tempMessage.paramsLen);

    IsCmdCallBack[tempMessage.id] = false;
    
    MessageWrite(&gSerialProtocolHandler, &tempMessage);
    //wait command Execution
    while (IsCmdCallBack[tempMessage.id] == false ) {
        ProtocolProcess();
    }
    //(*queuedCmdIndex)++;
    return true;
}

/*********************************************************************************************************
** Function name:       SetPTPJumpParams
** Descriptions:        Set the gate type parameter
** Input parameters:    ptpJumpParams,isQueued
** Output parameters:   queuedCmdIndex
** Returned value:      true
*********************************************************************************************************/
int SetPTPJumpParams(PTPJumpParams *ptpJumpParams, bool isQueued, uint64_t *queuedCmdIndex)
{
    Message tempMessage;
    
    memset(&tempMessage, 0, sizeof(Message));
    tempMessage.id = ProtocolPTPJumpParams;
    tempMessage.rw = true;
    tempMessage.isQueued = isQueued;
    tempMessage.paramsLen = sizeof(PTPJumpParams);
    memcpy(tempMessage.params,(uint8_t *)ptpJumpParams, tempMessage.paramsLen);

    IsCmdCallBack[tempMessage.id] = false;
    
    MessageWrite(&gSerialProtocolHandler, &tempMessage);
    //wait command Execution
    while (IsCmdCallBack[tempMessage.id] == false ) {
        ProtocolProcess();
    }
    //(*queuedCmdIndex)++;
    return true;
}

/*********************************************************************************************************
** Function name:       SetPTPCommonParams
** Descriptions:        Set point common parameters
** Input parameters:    ptpCommonParams,isQueued
** Output parameters:   queuedCmdIndex
** Returned value:      true
*********************************************************************************************************/
int SetPTPCommonParams(PTPCommonParams *ptpCommonParams, bool isQueued, uint64_t *queuedCmdIndex)
{
    Message tempMessage;
    
    memset(&tempMessage, 0, sizeof(Message));
    tempMessage.id = ProtocolPTPCommonParams;
    tempMessage.rw = true;
    tempMessage.isQueued = isQueued;
    tempMessage.paramsLen = sizeof(PTPCommonParams);
    memcpy(tempMessage.params, (uint8_t *)ptpCommonParams, tempMessage.paramsLen);

    IsCmdCallBack[tempMessage.id] = false;
    
    MessageWrite(&gSerialProtocolHandler, &tempMessage);
    //wait command Execution
    while (IsCmdCallBack[tempMessage.id] == false ) {
        ProtocolProcess();
    }
    //(*queuedCmdIndex)++;
    return true;
}
/*********************************************************************************************************
** Function name:       SetPTPCommonParams
** Descriptions:        Set point common parameters
** Input parameters:    ptpCommonParams,isQueued
** Output parameters:   queuedCmdIndex
** Returned value:      true
*********************************************************************************************************/
int SetPTPLParams(PTPLParams *ptpLParams, bool isQueued, uint64_t *queuedCmdIndex)
{
    Message tempMessage;
    
    memset(&tempMessage, 0, sizeof(Message));
    tempMessage.id = ProtocolPTPLParams;
    tempMessage.rw = true;
    tempMessage.isQueued = isQueued;
    tempMessage.paramsLen = sizeof(PTPLParams);
    memcpy(tempMessage.params, (uint8_t *)ptpLParams, tempMessage.paramsLen);

    IsCmdCallBack[tempMessage.id] = false;
    
    MessageWrite(&gSerialProtocolHandler, &tempMessage);
    //wait command Execution
    while (IsCmdCallBack[tempMessage.id] == false ) {
        ProtocolProcess();
    }
    //(*queuedCmdIndex)++;
    return true;
}
/*********************************************************************************************************
** Function name:       SetPTPCmd
** Descriptions:        Execute the position function
** Input parameters:    ptpCmd,isQueued
** Output parameters:   queuedCmdIndex
** Returned value:      true
*********************************************************************************************************/

int SetPTPCmd(PTPCmd *ptpCmd, bool isQueued, uint64_t *queuedCmdIndex)
{
    Message tempMessage;
    
    memset(&tempMessage, 0, sizeof(Message));
    tempMessage.id = ProtocolPTPCmd;
    tempMessage.rw = true;
    tempMessage.isQueued = isQueued;
    tempMessage.paramsLen = sizeof(PTPCmd);
    memcpy(tempMessage.params, (uint8_t *)ptpCmd, tempMessage.paramsLen);

    IsCmdCallBack[tempMessage.id] = false;
    MessageWrite(&gSerialProtocolHandler, &tempMessage);
    //wait command Execution
    
    uint32_t temppointer;
    while (IsCmdCallBack[tempMessage.id] == false ) {
        temppointer = ProtocolProcess();
    }

    //copy params receive
    memcpy(&gQueuedCmdWriteIndex,temppointer,sizeof(uint64_t));
    return true;
}
/*********************************************************************************************************
** Function name:       SetPTPCmd
** Descriptions:        Execute the position function
** Input parameters:    ptpCmd,isQueued
** Output parameters:   queuedCmdIndex
** Returned value:      true
*********************************************************************************************************/

int SetPTPCmdWithL(PTPWithLCmd *ptpWithLCmd, bool isQueued, uint64_t *queuedCmdIndex)
{
    Message tempMessage;
    
    memset(&tempMessage, 0, sizeof(Message));
    tempMessage.id = ProtocolPTPWithLCmd;
    tempMessage.rw = true;
    tempMessage.isQueued = isQueued;
    tempMessage.paramsLen = sizeof(PTPWithLCmd);
    memcpy(tempMessage.params, (uint8_t *)ptpWithLCmd, tempMessage.paramsLen);

    IsCmdCallBack[tempMessage.id] = false;
    MessageWrite(&gSerialProtocolHandler, &tempMessage);
    //wait command Execution
    
    uint32_t temppointer;
    while (IsCmdCallBack[tempMessage.id] == false ) {
        temppointer = ProtocolProcess();
    }

    //copy params receive
    memcpy(&gQueuedCmdWriteIndex,temppointer,sizeof(uint64_t));
    return true;
}
/*********************************************************************************************************
** 
** 
**                                          EIO

*********************************************************************************************************/
/*********************************************************************************************************
** Function name:       SetIOMultiplexing
** Descriptions:        SetIOMultiplexing
** Input parameters:    *iOConfig,endEffectorParams, isQueued
** Output parameters:   queuedCmdIndex
** Returned value:      true
*********************************************************************************************************/
int SetIOMultiplexing(IOConfig *iOConfig,bool isQueued, uint64_t *queuedCmdIndex)
{
    Message tempMessage;

    memset(&tempMessage, 0, sizeof(Message));
    tempMessage.id = ProtocolIOMultiplexing;
    tempMessage.rw = true;
    tempMessage.isQueued = isQueued;
    tempMessage.paramsLen = sizeof(IOConfig);
    memcpy(tempMessage.params, (uint8_t *)iOConfig, tempMessage.paramsLen);

    IsCmdCallBack[tempMessage.id] = false;
    
    MessageWrite(&gSerialProtocolHandler, &tempMessage);
    //wait command Execution
    uint32_t temppointer;
    while (IsCmdCallBack[tempMessage.id] == false ) {
        temppointer = ProtocolProcess();
    }

    //copy params receive
    //memcpy(&gDeviceID,temppointer,3*sizeof(uint32_t));

    //(*queuedCmdIndex)++;
    return true;
}

/*********************************************************************************************************
** Function name:       
** Descriptions:        
** Input parameters:     isQueued
** Output parameters:   queuedCmdIndex
** Returned value:      true
*********************************************************************************************************/
int SetIODO(EIODO *eIODO,bool isQueued, uint64_t *queuedCmdIndex)
{
    Message tempMessage;

    memset(&tempMessage, 0, sizeof(Message));
    tempMessage.id = ProtocolIODO;
    tempMessage.rw = true;
    tempMessage.isQueued = isQueued;
    tempMessage.paramsLen = sizeof(eIODO);
    memcpy(tempMessage.params, (uint8_t *)eIODO, tempMessage.paramsLen);

    IsCmdCallBack[tempMessage.id] = false;
    
    MessageWrite(&gSerialProtocolHandler, &tempMessage);
    //wait command Execution
    uint32_t temppointer;
    while (IsCmdCallBack[tempMessage.id] == false ) {
        temppointer = ProtocolProcess();
    }

    //copy params receive
    //memcpy(&gDeviceID,temppointer,3*sizeof(uint32_t));

    //(*queuedCmdIndex)++;
    return true;
}

/*********************************************************************************************************
** Function name:       
** Descriptions:        
** Input parameters:     isQueued
** Output parameters:   queuedCmdIndex
** Returned value:      true
*********************************************************************************************************/
int SetIOPWM(EIOPWM *eIOPWM,bool isQueued, uint64_t *queuedCmdIndex)
{
    Message tempMessage;

    memset(&tempMessage, 0, sizeof(Message));
    tempMessage.id = ProtocolIOPWM;
    tempMessage.rw = true;
    tempMessage.isQueued = isQueued;
    tempMessage.paramsLen = sizeof(EIOPWM);
    memcpy(tempMessage.params, (uint8_t *)eIOPWM, tempMessage.paramsLen);

    IsCmdCallBack[tempMessage.id] = false;
    
    MessageWrite(&gSerialProtocolHandler, &tempMessage);
    //wait command Execution
    uint32_t temppointer;
    while (IsCmdCallBack[tempMessage.id] == false ) {
        temppointer = ProtocolProcess();
    }

    //copy params receive
    //memcpy(&gDeviceID,temppointer,3*sizeof(uint32_t));

    //(*queuedCmdIndex)++;
    return true;
}
/*********************************************************************************************************
** Function name:       
** Descriptions:        
** Input parameters:     isQueued
** Output parameters:   queuedCmdIndex
** Returned value:      true
*********************************************************************************************************/
int GetIODI(EIODI *eIODI,bool isQueued, uint64_t *queuedCmdIndex)
{
    Message tempMessage;

    memset(&tempMessage, 0, sizeof(Message));
    tempMessage.id = ProtocolIODI;
    tempMessage.rw = false;
    tempMessage.isQueued = isQueued;
    tempMessage.paramsLen = sizeof(EIODI);
    tempMessage.params[0] = eIODI->address;
    //memcpy(tempMessage.params, (uint8_t *)eIOPWM, tempMessage.paramsLen);

    IsCmdCallBack[tempMessage.id] = false;
    
    MessageWrite(&gSerialProtocolHandler, &tempMessage);
    //wait command Execution
    uint32_t temppointer;
    while (IsCmdCallBack[tempMessage.id] == false ) {
        temppointer = ProtocolProcess();
    }

    //copy params receive
    memcpy(eIODI,temppointer,sizeof(EIODI));
    printf("index:%d,value:%d\r\n",eIODI->address,eIODI->value);
    //(*queuedCmdIndex)++;
    return true;
}
/*********************************************************************************************************
** Function name:       
** Descriptions:        
** Input parameters:     isQueued
** Output parameters:   queuedCmdIndex
** Returned value:      true
*********************************************************************************************************/
int GetIOADC(EIOADC *eIOADC,bool isQueued, uint64_t *queuedCmdIndex)
{
    Message tempMessage;

    memset(&tempMessage, 0, sizeof(Message));
    tempMessage.id = ProtocolIOADC;
    tempMessage.rw = false;
    tempMessage.isQueued = isQueued;
    tempMessage.paramsLen = sizeof(EIOADC);
    tempMessage.params[0] = eIOADC->address;
    memcpy(tempMessage.params, (uint8_t *)eIOADC, tempMessage.paramsLen);

    IsCmdCallBack[tempMessage.id] = false;
    
    MessageWrite(&gSerialProtocolHandler, &tempMessage);
    //wait command Execution
    uint32_t temppointer;
    while (IsCmdCallBack[tempMessage.id] == false ) {
        temppointer = ProtocolProcess();
    }

    //copy params receive
    memcpy(eIOADC,temppointer,sizeof(EIOADC));
    //(*queuedCmdIndex)++;
    return true;
}
/*********************************************************************************************************
** Function name:       
** Descriptions:        
** Input parameters:     isQueued
** Output parameters:   queuedCmdIndex
** Returned value:      true
*********************************************************************************************************/
int SetEMotor(EMotor *eMotor,bool isQueued, uint64_t *queuedCmdIndex)
{
    Message tempMessage;

    memset(&tempMessage, 0, sizeof(Message));
    tempMessage.id = ProtocolEMotor;
    tempMessage.rw = true;
    tempMessage.isQueued = isQueued;
    tempMessage.paramsLen = sizeof(EMotor);
    memcpy(tempMessage.params, (uint8_t *)eMotor, tempMessage.paramsLen);

    IsCmdCallBack[tempMessage.id] = false;
    
    MessageWrite(&gSerialProtocolHandler, &tempMessage);
    //wait command Execution
    uint32_t temppointer;
    while (IsCmdCallBack[tempMessage.id] == false ) {
        temppointer = ProtocolProcess();
    }

    //copy params receive
    //memcpy(&gDeviceID,temppointer,3*sizeof(uint32_t));

    //(*queuedCmdIndex)++;
    return true;
}
/*********************************************************************************************************
** Function name:       
** Descriptions:        
** Input parameters:     isQueued
** Output parameters:   queuedCmdIndex
** Returned value:      true
*********************************************************************************************************/
int SetEMotorS(EMotorS *eMotorS,bool isQueued, uint64_t *queuedCmdIndex)
{
    Message tempMessage;

    memset(&tempMessage, 0, sizeof(Message));
    tempMessage.id = ProtocolEMotorS;
    tempMessage.rw = true;
    tempMessage.isQueued = isQueued;
    tempMessage.paramsLen = sizeof(EMotorS);
    memcpy(tempMessage.params, (uint8_t *)eMotorS, tempMessage.paramsLen);

    IsCmdCallBack[tempMessage.id] = false;
    
    MessageWrite(&gSerialProtocolHandler, &tempMessage);
    //wait command Execution
    uint32_t temppointer;
    while (IsCmdCallBack[tempMessage.id] == false ) {
        temppointer = ProtocolProcess();
    }

    //copy params receive
    //memcpy(&gDeviceID,temppointer,3*sizeof(uint32_t));

    //(*queuedCmdIndex)++;
    return true;
}
/*********************************************************************************************************
** Function name:       
** Descriptions:        
** Input parameters:     isQueued
** Output parameters:   queuedCmdIndex
** Returned value:      true
*********************************************************************************************************/
int SetColorSensor(ColorSensor *colorSensor,bool isQueued, uint64_t *queuedCmdIndex)
{
    Message tempMessage;

    memset(&tempMessage, 0, sizeof(Message));
    tempMessage.id = ProtocolColorSensor;
    tempMessage.rw = true;
    tempMessage.isQueued = isQueued;
    tempMessage.paramsLen = sizeof(ColorSensor) - 3;
    memcpy(tempMessage.params, (uint8_t *)colorSensor, tempMessage.paramsLen);

    IsCmdCallBack[tempMessage.id] = false;
   
    MessageWrite(&gSerialProtocolHandler, &tempMessage);
    
    //wait command Execution
    uint32_t temppointer;
    while (IsCmdCallBack[tempMessage.id] == false ) {
     // printf("!!!!!!\r\n");
        temppointer = ProtocolProcess();
    }

    //copy params receive
    //memcpy(&gDeviceID,temppointer,3*sizeof(uint32_t));
    //(*queuedCmdIndex)++;
    return true;
}
/*********************************************************************************************************
** Function name:       
** Descriptions:        
** Input parameters:     isQueued
** Output parameters:   queuedCmdIndex
** Returned value:      true
*********************************************************************************************************/
int GetColorSensor(ColorSensor *colorSensor,bool isQueued, uint64_t *queuedCmdIndex)
{
    Message tempMessage;

    memset(&tempMessage, 0, sizeof(Message));
    tempMessage.id = ProtocolColorSensor;
    tempMessage.rw = false;
    tempMessage.isQueued = isQueued;
    tempMessage.paramsLen = 0;
    //memcpy(tempMessage.params, sysParams->iRSwitch.port, tempMessage.paramsLen);

    IsCmdCallBack[tempMessage.id] = false;
    
    MessageWrite(&gSerialProtocolHandler, &tempMessage);
    //wait command Execution
    uint32_t temppointer;
    while (IsCmdCallBack[tempMessage.id] == false ) {
        temppointer = ProtocolProcess();
    }

    //copy params receive
    memcpy(&colorSensor->r,temppointer,sizeof(ColorSensor)-2);

    //(*queuedCmdIndex)++;
    return true;
}
/*********************************************************************************************************
** Function name:       
** Descriptions:        
** Input parameters:     isQueued
** Output parameters:   queuedCmdIndex
** Returned value:      true
*********************************************************************************************************/
int SetIRSwitch(IRSwitch *iRSwitch,bool isQueued, uint64_t *queuedCmdIndex)
{
    Message tempMessage;

    memset(&tempMessage, 0, sizeof(Message));
    tempMessage.id = ProtocolIRSwitch;
    tempMessage.rw = true;
    tempMessage.isQueued = isQueued;
    tempMessage.paramsLen = sizeof(IRSwitch) - 1;
    memcpy(tempMessage.params, (uint8_t *)iRSwitch, tempMessage.paramsLen);

    IsCmdCallBack[tempMessage.id] = false;
    
    MessageWrite(&gSerialProtocolHandler, &tempMessage);
    //wait command Execution
    uint32_t temppointer;
    while (IsCmdCallBack[tempMessage.id] == false ) {
        temppointer = ProtocolProcess();
    }

    //copy params receive
    //memcpy(&gDeviceID,temppointer,3*sizeof(uint32_t));

    //(*queuedCmdIndex)++;
    return true;
}
/*********************************************************************************************************
** Function name:       
** Descriptions:        
** Input parameters:     isQueued
** Output parameters:   queuedCmdIndex
** Returned value:      true
*********************************************************************************************************/
int GetIRSwitch(IRSwitch *iRSwitch,bool isQueued, uint64_t *queuedCmdIndex)
{
    Message tempMessage;

    memset(&tempMessage, 0, sizeof(Message));
    tempMessage.id = ProtocolIRSwitch;
    tempMessage.rw = false;
    tempMessage.isQueued = isQueued;
    tempMessage.paramsLen = sizeof(iRSwitch->port);
    memcpy(tempMessage.params, &iRSwitch->port, tempMessage.paramsLen);

    IsCmdCallBack[tempMessage.id] = false;
    
    MessageWrite(&gSerialProtocolHandler, &tempMessage);
    //wait command Execution
    uint32_t temppointer;
    while (IsCmdCallBack[tempMessage.id] == false ) {
        temppointer = ProtocolProcess();
    }

    //copy params receive
    memcpy(&iRSwitch->value,temppointer,sizeof(iRSwitch->value));

    //(*queuedCmdIndex)++;
    return true;
}
/*********************************************************************************************************
** 
** 
**                      Lost Step
** 
** 
*********************************************************************************************************/
/*********************************************************************************************************
** Function name:       
** Descriptions:        
** Input parameters:     isQueued
** Output parameters:   queuedCmdIndex
** Returned value:      true
*********************************************************************************************************/
int SetLostStepSetValue(SysParams *sysParams,bool isQueued, uint64_t *queuedCmdIndex)
{
    Message tempMessage;

    memset(&tempMessage, 0, sizeof(Message));
    tempMessage.id = ProtocolLostStepSet;
    tempMessage.rw = true;
    tempMessage.isQueued = isQueued;
    tempMessage.paramsLen = sizeof(sysParams->lostStepValue);
    memcpy(tempMessage.params, &sysParams->lostStepValue, tempMessage.paramsLen);

    IsCmdCallBack[tempMessage.id] = false;
    
    MessageWrite(&gSerialProtocolHandler, &tempMessage);
    //wait command Execution
    uint32_t temppointer;
    while (IsCmdCallBack[tempMessage.id] == false ) {
        temppointer = ProtocolProcess();
    }

    //copy params receive
    //memcpy(&gDeviceID,temppointer,3*sizeof(uint32_t));

    //(*queuedCmdIndex)++;
    return true;
}
/*********************************************************************************************************
** Function name:       
** Descriptions:        
** Input parameters:     isQueued
** Output parameters:   queuedCmdIndex
** Returned value:      true
*********************************************************************************************************/
int SetMotorPulse(PulseCmd *pulseCmd,bool isQueued, uint64_t *queuedCmdIndex)
{
    Message tempMessage;

    memset(&tempMessage, 0, sizeof(Message));
    tempMessage.id = ProtocolFunctionPulseMode;
    tempMessage.rw = true;
    tempMessage.isQueued = isQueued;
    tempMessage.paramsLen = sizeof(PulseCmd);
    memcpy(tempMessage.params, pulseCmd, tempMessage.paramsLen);

    IsCmdCallBack[tempMessage.id] = false;
    
    MessageWrite(&gSerialProtocolHandler, &tempMessage);
    //wait command Execution
    uint32_t temppointer;
    while (IsCmdCallBack[tempMessage.id] == false ) {
        temppointer = ProtocolProcess();
    }

    //copy params receive
    //memcpy(&gDeviceID,temppointer,3*sizeof(uint32_t));

    //(*queuedCmdIndex)++;
    memcpy(&gQueuedCmdWriteIndex,temppointer,sizeof(uint64_t));
    return true;
}
/*********************************************************************************************************
** Function name:       
** Descriptions:        
** Input parameters:     isQueued
** Output parameters:   queuedCmdIndex
** Returned value:      true
*********************************************************************************************************/
int SetLostStepDetect(bool isQueued, uint64_t *queuedCmdIndex)
{
    Message tempMessage;

    memset(&tempMessage, 0, sizeof(Message));
    tempMessage.id = ProtocolLostStepDetec;
    tempMessage.rw = true;
    tempMessage.isQueued = isQueued;
    tempMessage.paramsLen = 0;
    //memcpy(tempMessage.params, (uint8_t *)sysParams.LostStepValue, tempMessage.paramsLen);

    IsCmdCallBack[tempMessage.id] = false;
    
    MessageWrite(&gSerialProtocolHandler, &tempMessage);
    //wait command Execution
    uint32_t temppointer;
    while (IsCmdCallBack[tempMessage.id] == false ) {
        temppointer = ProtocolProcess();
    }

    //copy params receive
    //memcpy(&temp,temppointer,3*sizeof(uint32_t));

    //(*queuedCmdIndex)++;
    return true;
}
/*********************************************************************************************************
** Function name:       GetQueuedCmdCurrentIndex                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
** Descriptions:        check current index
** Input parameters:    noQueued
** Output parameters:   queuedCmdIndex
** Returned value:      true
*********************************************************************************************************/
int  GetQueuedCmdCurrentIndex(bool isQueued, uint64_t *queuedCmdIndex)
{
    Message tempMessage;
    Message message;

    memset(&tempMessage, 0, sizeof(Message));
    tempMessage.id = ProtocolQueuedCmdCurrentIndex;
    tempMessage.rw = false;
    tempMessage.isQueued = isQueued;
    tempMessage.paramsLen = 0;

    IsCmdCallBack[tempMessage.id] = false;
    MessageWrite(&gSerialProtocolHandler, &tempMessage);
    
    //wait command Execution
    uint32_t temppointer;
    while (IsCmdCallBack[tempMessage.id] == false ) {
        temppointer = ProtocolProcess();
        static uint32_t timeoutcounter = millis();
        if(millis() - timeoutcounter>1000){
            timeoutcounter = millis();
            MessageWrite(&gSerialProtocolHandler, &tempMessage);
        }
    }
    memcpy(&gQueuedCmdCurrentIndex,temppointer,sizeof(uint64_t));
    return true;
}
/*********************************************************************************************************
** Function name:       GetQueuedCmdCurrentIndex                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
** Descriptions:        check current index
** Input parameters:    noQueued
** Output parameters:   queuedCmdIndex
** Returned value:      true
*********************************************************************************************************/
int GetQueuedCmdLeftSpace( bool isQueued, uint64_t *queuedCmdIndex)
{
    Message tempMessage;

    memset(&tempMessage, 0, sizeof(Message));
    tempMessage.id = ProtocolQueuedCmdLeftSpace;
    tempMessage.rw = false;
    tempMessage.isQueued = isQueued;
    tempMessage.paramsLen = 0 ;

    MessageWrite(&gSerialProtocolHandler, &tempMessage);
    uint32_t temppointer;
    while (IsCmdCallBack[tempMessage.id] == false ) {
        temppointer = ProtocolProcess();
        
    }
    IsCmdCallBack[tempMessage.id] = false;
    uint32_t temp;
    memcpy(&gQueueCmdLeftSpace,temppointer,sizeof(uint32_t));
    printf("gQueueCmdLeftSpace:%d\r\n",gQueueCmdLeftSpace);
    //(*queuedCmdIndex)++;
    return true;
}
/*********************************************************************************************************
** Function name:       GetQueuedCmdCurrentIndex                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
** Descriptions:        check current index
** Input parameters:    noQueued
** Output parameters:   queuedCmdIndex
** Returned value:      true
*********************************************************************************************************/
int SetQueuedCmdStartExec( bool isQueued, uint64_t *queuedCmdIndex)
{
    Message tempMessage;

    memset(&tempMessage, 0, sizeof(Message));
    tempMessage.id = ProtocolQueuedCmdStartExec;
    tempMessage.rw = false;
    tempMessage.isQueued = isQueued;
    tempMessage.paramsLen = 0 ;

    MessageWrite(&gSerialProtocolHandler, &tempMessage);

    //(*queuedCmdIndex)++;
    return true;
}
/*********************************************************************************************************
** Function name:       GetQueuedCmdCurrentIndex                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
** Descriptions:        check current index
** Input parameters:    noQueued
** Output parameters:   queuedCmdIndex
** Returned value:      true
*********************************************************************************************************/
int SetQueuedCmdStopExec( bool isQueued, uint64_t *queuedCmdIndex)
{
    Message tempMessage;

    memset(&tempMessage, 0, sizeof(Message));
    tempMessage.id = ProtocolQueuedCmdStopExec ;
    tempMessage.rw = false;
    tempMessage.isQueued = isQueued;
    tempMessage.paramsLen = 0 ;

    MessageWrite(&gSerialProtocolHandler, &tempMessage);

    //(*queuedCmdIndex)++;
    return true;
}



