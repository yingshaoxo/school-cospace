#ifndef COMMAND_H
#define COMMAND_H

/****************************************Copyright(c)*****************************************************
**                            Shenzhen Yuejiang Technology Co., LTD.
**
**                                 http://www.dobot.cc
**
**--------------File Info---------------------------------------------------------------------------------
** File name:           command.h
** Latest modified Date:
** Latest Version:      V1.0.0
** Descriptions:
**
**--------------------------------------------------------------------------------------------------------
** Created by:          Edward
** Created date:        2017-8-15
** Version:             V1.0.0
** Descriptions:        Data definition
**--------------------------------------------------------------------------------------------------------
*********************************************************************************************************/

#include <stdint.h>
#include "ProtocolID.h"
#include "type.h"

/*********************************************************************************************************
** Device function
*********************************************************************************************************/
extern int GetDeviceID(uint32_t *devicetime,bool isQueued, uint64_t *queuedCmdIndex);
extern int GetDeviceTime(uint32_t *deviceTime,bool isQueued, uint64_t *queuedCmdIndex);
extern int SetDeviceWIthL(bool isWithL,bool isQueued, uint64_t *queuedCmdIndex);
/*********************************************************************************************************
** Pose
*********************************************************************************************************/
extern int GetPose(Pose *pose,bool isQueued, uint64_t *queuedCmdIndex);
extern int GetPoseL(float *poseL,bool isQueued, uint64_t *queuedCmdIndex);
/*********************************************************************************************************
** Home
*********************************************************************************************************/
extern int SetHomeCmd(bool isQueued, uint64_t *queuedCmdIndex);
extern int SetHomeParamsCmd(Pose *pose,bool isQueued, uint64_t *queuedCmdIndex);
extern uint32_t GetHomeParamsCmd(bool isQueued, uint64_t *queuedCmdIndex);
/*********************************************************************************************************
** End effector function
*********************************************************************************************************/
extern int SetEndEffectorParams(EndEffectorParams *endEffectorParams, bool isQueued, uint64_t *queuedCmdIndex);
extern int SetEndEffectorLaser(bool ison, bool isQueued, uint64_t *queuedCmdIndex);
extern int SetEndEffectorSuctionCup(bool issuck, bool isQueued, uint64_t *queuedCmdIndex);
extern int SeEndEffectorGritpper(EndEffectorGripper *endEffectorGripper, bool isQueued, uint64_t *queuedCmdIndex);
/*********************************************************************************************************
** jog function
*********************************************************************************************************/
extern int SetJOGJointParams(JOGJointParams *jogJointParams, bool isQueued, uint64_t *queuedCmdIndex);
extern int SetJOGCommonParams(JOGCommonParams *jogCommonParams, bool isQueued, uint64_t *queuedCmdIndex);
extern int SetJOGCmd(JOGCmd *jogCmd, bool isQueued, uint64_t *queuedCmdIndex);
/*********************************************************************************************************
** PTP function
*********************************************************************************************************/
extern int SetPTPJointParams(PTPJointParams *ptpJointParams, bool isQueued, uint64_t *queuedCmdIndex);
extern int SetPTPCoordinateParams(PTPCoordinateParams *ptpCoordinateParams, bool isQueued, uint64_t *queuedCmdIndex);
extern int SetPTPJumpParams(PTPJumpParams *ptpJumpParams, bool isQueued, uint64_t *queuedCmdIndex);
extern int SetPTPCommonParams(PTPCommonParams *ptpCommonParams, bool isQueued, uint64_t *queuedCmdIndex);
extern int SetPTPCmd(PTPCmd *ptpCmd, bool isQueued, uint64_t *queuedCmdIndex);
extern int SetPTPLParams(PTPLParams *ptpLParams, bool isQueued, uint64_t *queuedCmdIndex);
extern int SetPTPCmdWithL(PTPWithLCmd *ptpWithLCmd, bool isQueued, uint64_t *queuedCmdIndex);
/*********************************************************************************************************
** EIO function
*********************************************************************************************************/
extern int SetIOMultiplexing(IOConfig *iOConfig,bool isQueued, uint64_t *queuedCmdIndex);
extern int SetIODO(EIODO *eIODO,bool isQueued, uint64_t *queuedCmdIndex);
extern int SetIOPWM(EIOPWM *eIOPWM,bool isQueued, uint64_t *queuedCmdIndex);
extern int GetIODI(EIODI *eIODI,bool isQueued, uint64_t *queuedCmdIndex);
extern int GetIOADC(EIOADC *eIOADC,bool isQueued, uint64_t *queuedCmdIndex);
extern int SetEMotor(EMotor *eMotor,bool isQueued, uint64_t *queuedCmdIndex);
extern int SetEMotorS(EMotorS *eMotorS,bool isQueued, uint64_t *queuedCmdIndex);
extern int SetColorSensor(ColorSensor *colorSensor,bool isQueued, uint64_t *queuedCmdIndex);
extern int GetColorSensor(ColorSensor *colorSensor,bool isQueued, uint64_t *queuedCmdIndex);
extern int SetIRSwitch(IRSwitch *iRSwitch,bool isQueued, uint64_t *queuedCmdIndex);
extern int GetIRSwitch(IRSwitch *iRSwitch,bool isQueued, uint64_t *queuedCmdIndex);
/*********************************************************************************************************
** LostStep function
*********************************************************************************************************/
extern int SetLostStepSetValue(SysParams *sysParams,bool isQueued, uint64_t *queuedCmdIndex);
extern int SetLostStepDetect(bool isQueued, uint64_t *queuedCmdIndex);
//temp test
extern int SetMotorPulse(PulseCmd *pulseCmd,bool isQueued, uint64_t *queuedCmdIndex);
/*********************************************************************************************************
** QueueCmd function
*********************************************************************************************************/
extern int GetQueuedCmdCurrentIndex(bool isQueued, uint64_t *queuedCmdIndex);
extern int GetQueuedCmdLeftSpace(bool isQueued, uint64_t *queuedCmdIndex);
extern int SetQueuedCmdStartExec( bool isQueued, uint64_t *queuedCmdIndex);
extern int SetQueuedCmdStopExec( bool isQueued, uint64_t *queuedCmdIndex);
/*

*/
//extern int  WaitCmdExecution(Message &tempMessage);
#endif



