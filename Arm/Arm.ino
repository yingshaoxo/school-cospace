/****************************************Copyright(c)*****************************************************
**                            Shenzhen Yuejiang Technology Co., LTD.
**
**                                 http://www.dobot.cc
**--------------------------------------------------------------------------------------------------------
** Modify by:           
** Modified date:       2018-5-15
** Version:             V1.1.0
** Descriptions:        Visual recognition for DOBOT Magician
**--------------------------------------------------------------------------------------------------------
*********************************************************************************************************/


// global setting
bool debug = false;
bool setcam = false;

int red_plan = 2;
int yellow_plan = 2;
int green_plan = 2;
int blue_plan = 1;

float red_x_adds = -15.0*0.5;
float red_y_adds = 10.0*0.5;
float yellow_x_adds = -15.0*0.5;
float yellow_y_adds = 10.0*0.5;
float green_x_adds = -15.0*0.5;
float green_y_adds = 10.0*0.5;
float blue_x_adds = -15.0*0.5;
float blue_y_adds = 10.0*0.5;

/*
float red_x_adds = 0;
float red_y_adds = 0;
float yellow_x_adds = 0;
float yellow_y_adds = 0;
float green_x_adds = 0;
float green_y_adds = 0;
float blue_x_adds = 0;
float blue_y_adds = 0;
*/

const int block_width = 10;
const int block_height = 10;

int sum;
int position[6][3] = {
{-30, 301, 43},
{-26, 260, 43},
{7, 302, 43},
{10, 265, 43},
{46, 306, 43},
{47, 264, 43},
};

/*
#define block_width  10
#define block_height 10
*/
int light_time = 0;


#include "Dobot.h"
#include "command.h"
#include "FlexiTimer2.h"
#include "Message.h"
#include "Packet.h"
#include "Protocol.h"
#include "ProtocolDef.h"
#include "ProtocolID.h"
#include "RingBuffer.h"
#include "symbol.h"
#include "type.h"
#include "Wire.h"

#define PixyCmucamV1 0
#define PixyCmucamV2 1

#if PixyCmucamV1
    #include "Pixy.h"
    #include "PixyI2C.h"
    #include "TPixy.h"
#endif

#if PixyCmucamV2
    #include "Pixy2.h"
#endif

#if PixyCmucamV1
    PixyI2C pixy;                                                                            //Structural variable
#endif

#if PixyCmucamV2
    Pixy2 pixy;   
#endif

uint16_t blocks,signature,width,height;
int gRed = 0,gYellow = 0,gGreen = 0,gBlue = 0;                                            //blocks count
String comdata = "";
int load = 0;
int unload = 0;
int BlockCount = 0;

#if PixyCmucamV1
    //the coordinates of pixy 
    float dobotPoint[9] = {                                                                   //the coordinates of three blocks for Magician
    276, 275, 227,
    23, -30, -27,
    1, 1, 1
    };

    float pixyPoint[9] = {                                                                    //the coordinates of three blocks for Pixy
    292, 248, 280,
    -39, -29, 9,
    1, 1, 1
    };
#endif

#if PixyCmucamV2
    //the coordinates of pixy 2
    float dobotPoint[9] = {
    236, 277, 242,       
    28, 0, -28,
    1, 1, 1
    };

    float pixyPoint[9] = {
        95, 149, 212,
        168, 78, 147,
        1, 1, 1
    };
#endif

float RT[9] = {                                                                           //basis matrix
    0, 0, 0,
    0, 0, 0,
    0, 0, 0
};

float Coordinate[2]= {0};                                                                 //The X and Y coordinates of the target block

void CalcInvMat(float *Mat, float *InvMat)                                                //Inverse matrix
{
    int i = 0;
    double Det = 0.0;
    Det = Mat[0] * (Mat[4] * Mat[8] - Mat[5] * Mat[7]) - Mat[3] * (Mat[1] * Mat[8] - Mat[2] * Mat[7]) + Mat[6] * (Mat[1] * Mat[5] - Mat[2] * Mat[4]);
    InvMat[0] = Mat[4] * Mat[8] - Mat[5] * Mat[7];
    InvMat[1] = Mat[2] * Mat[7] - Mat[1] * Mat[8];
    InvMat[2] = Mat[1] * Mat[5] - Mat[2] * Mat[4];
    InvMat[3] = Mat[5] * Mat[6] - Mat[3] * Mat[8];
    InvMat[4] = Mat[0] * Mat[8] - Mat[2] * Mat[6];
    InvMat[5] = Mat[3] * Mat[2] - Mat[0] * Mat[5];
    InvMat[6] = Mat[3] * Mat[7] - Mat[4] * Mat[6];
    InvMat[7] = Mat[1] * Mat[6] - Mat[7] * Mat[0];
    InvMat[8] = Mat[0] * Mat[4] - Mat[3] * Mat[1];
    for (i = 0; i < 9; i++) {
        InvMat[i] = InvMat[i] / Det;
    }
}

void MatMultiMat(float *Mat1, float *Mat2, float *Result)
{
    int i = 0;
    int j = 0;
    int k = 0;
    int Value = 0;

    for (i = 0; i < 9; i++) {
        Result[i] = 0;
    }
    for (i = 0; i < 3; i++) {
        for (j = 0; j < 3; j++) {
            for (k = 0; k < 3; k++) {
                Result[i * 3 + j] += Mat1[i * 3 + k] * Mat2[k * 3 + j];
            }
        }
    }
}

void transForm(float x, float y) {                                                         
    Coordinate[0] = (RT[0] * x) + (RT[1] * y) + (RT[2] * 1);
    Coordinate[1] = (RT[3] * x) + (RT[4] * y) + (RT[5] * 1);
}

void put_donw() {
    Dobot_SetPTPCmdEx(JUMP_XYZ, position[sum][0], position[sum][1], position[sum][2], 0); //Move to the setting position
    Dobot_SetEndEffectorSuctionCupEx(false); //Turn off air pump
    Dobot_SetPTPCmdEx(JUMP_XYZ, position[sum][0], position[sum][1], 50, 0); //Move to the setting position
}

void setup(){ 
    Serial.begin(115200);
    Serial2.begin(57600);

    if (setcam) {
        while (1) {
            delay(1000);
        }
    }

    Serial.print("Starting...\n");
    Dobot_Init();
    #if PixyCmucamV2
        pixy.init();
        pixy.setLamp(0, 0);
    #endif  
    Dobot_SetPTPJumpParamsEx(20);                                                                                                   //Set up the maximum height of the magician lift
    float inv_pixy[9] = {                                                                                                           //Initialization inverse matrix
    0, 0, 0,
    0, 0, 0,
    0, 0, 0
    };
    Dobot_SetEndEffectorParamsEx(59.7,0,0);
    CalcInvMat(pixyPoint, inv_pixy);
    MatMultiMat(dobotPoint, inv_pixy, RT);
    Dobot_SetPTPCommonParamsEx(100,100);
    Dobot_SetPTPCmdEx(MOVJ_XYZ, 163, 0, 0, 0);                                                                               

    pixy.setLamp(1, 0);
}

void loop() {
    while (Serial2.available() > 0 && !load && !unload) {
        int val;
        val = Serial2.read();
        if (val == '<') {
            while (val != '>') {
                comdata += char(val);
                delay(2);
                val = Serial2.read();
            }
            comdata += char(val);
            val = 0;
            //Serial.print("receive:");
            //Serial.print(comdata);
        } else if (Serial2.read() != '<') {
            return 0;
        }
        if (comdata.length() > 14) {
            comdata = "";
        }
        if (!comdata.compareTo("<!CSDB01LDoL!>")) {
            load = 1;
            Serial.println("      load");
            //Serial.println(comdata);      
        }
        if (!comdata.compareTo("<!CSDB02LDoU!>")) {
            unload = 1;
            Serial.println("      unload");
            //Serial.println(comdata);      
        }
        val = 0;
        comdata = "";
    }

    if (load == 1) {
        // Send signal to our car if we finished
        while ( ((BlockCount == 6) || ((gRed + gYellow + gGreen + gBlue) == (red_plan + yellow_plan + green_plan + blue_plan)) ) && (debug == false)) {
            Dobot_SetPTPCmdEx(MOVJ_XYZ, 163, 0, 0, 0);
            Serial2.write('<');
            Serial2.write('!');
            Serial2.write('C');
            Serial2.write('S');
            Serial2.write('L');
            Serial2.write('o');
            Serial2.write('g');
            Serial2.write('B');
            Serial2.write('L');
            Serial2.write('G');
            Serial2.write('o');
            Serial2.write('!');
            Serial2.write('>');

            pixy.setLamp(1, 0);
        }

        Dobot_SetPTPCmdEx(MOVJ_XYZ, 163, 0, 0, 0);
        char buf[16];
        #if PixyCmucamV1
            blocks = pixy.getBlocks(); //get the numbers of blocks
        #endif
        #if PixyCmucamV2
            pixy.ccc.getBlocks(); //get the numbers of blocks
            blocks = pixy.ccc.numBlocks;
        #endif

        sum = gRed + gYellow + gGreen + gBlue;

        if (blocks) {
            //#if PixyCmucam
            #if PixyCmucamV1
                transForm(pixy.blocks[0].x, pixy.blocks[0].y); //get the center X,Y coordinates of the target block
                signature = pixy.blocks[0].signature;
                width = pixy.blocks[0].width;
                height = pixy.blocks[0].height;
            #else
                transForm(pixy.ccc.blocks[0].m_x, pixy.ccc.blocks[0].m_y);
                signature = pixy.ccc.blocks[0].m_signature;
                width = pixy.ccc.blocks[0].m_width;
                height = pixy.ccc.blocks[0].m_height;
            #endif
            /*
            Serial.print(Coordinate[0]);
            Serial.print("  ");
            Serial.print(Coordinate[1]);
            Serial.print("\n");
            */

            Serial.print("\n--------------\n");
            Serial.print("width: ");
            Serial.print(width);
            Serial.print("\n");
            Serial.print("height: ");
            Serial.print(height);
            Serial.print("\n");

            if (light_time == 6) {
                pixy.setLamp(0, 0);
            } else if (light_time == 12) {
                pixy.setLamp(1, 0);
                light_time = 0;
            }
            light_time = light_time + 1;

            switch (signature) {
                case 1:
                    if( ((width >= block_width) || (height >= block_height)) && ((width < 70) || (height < 70)) )
                    {
                        if (gRed < red_plan) {
                            printf("1_red\n");
                            //suck Point
                            Dobot_SetPTPCmdEx(JUMP_XYZ, Coordinate[0] + red_x_adds, Coordinate[1] + red_y_adds, -45, 0); //The position of a target object 
                            Dobot_SetEndEffectorSuctionCupEx(true); //Turn on air pump
                            Dobot_SetPTPCmdEx(MOVL_XYZ, Coordinate[0] + red_x_adds, Coordinate[1] + red_y_adds, 30, 0);
                            //place Point
                            put_donw();
                            gRed++;
                            BlockCount++;
                            printf("gRed:");
                            Serial.println(gRed); //Lift to a certain height
                            break;
                        } else {
                            break;
                        }
                    } else {
                        break;
                    }
                case 2:
                    if( ((width >= block_width) || (height >= block_height)) && ((width < 70) || (height < 70)) )
                    {
                        if (gYellow < yellow_plan) {
                            printf("2_yellow\n");
                            //suck Point
                            Dobot_SetPTPCmdEx(JUMP_XYZ, Coordinate[0] + yellow_x_adds, Coordinate[1] + yellow_y_adds, -45, 0); //The position of a target object 
                            Dobot_SetEndEffectorSuctionCupEx(true); //Turn on air pump
                            Dobot_SetPTPCmdEx(MOVL_XYZ, Coordinate[0] + yellow_x_adds, Coordinate[1] + yellow_y_adds, 30, 0);
                            //place Point
                            put_donw();
                            gYellow++;
                            BlockCount++;
                            printf("gYellow:");
                            Serial.println(gYellow); //The number of the yellow block
                            break;
                        } else {
                            break;
                        }
                    } else {
                        break;
                    }
                case 3:
                    if( ((width >= block_width) || (height >= block_height)) && ((width < 70) || (height < 70)) )
                    {
                        if (gGreen < green_plan) {
                            printf("3_green\n");
                            //suck Point
                            Dobot_SetPTPCmdEx(JUMP_XYZ, Coordinate[0] + green_x_adds, Coordinate[1] + green_y_adds, -45, 0); //The position of a target object 
                            Dobot_SetEndEffectorSuctionCupEx(true);
                            Dobot_SetPTPCmdEx(MOVL_XYZ, Coordinate[0] + green_x_adds, Coordinate[1] + green_y_adds, 30, 0);
                            //place Point
                            put_donw();
                            gGreen++;
                            BlockCount++;
                            printf("gGreen:");
                            Serial.println(gGreen);
                            break;
                        } else {
                            break;
                        }
                    } else {
                        break;
                    }
                case 4:
                    if( ((width >= block_width) || (height >= block_height)) && ((width < 70) || (height < 70)) )
                    {
                        if (gBlue < blue_plan) {
                            printf("4_blue\n");
                            //suck Point
                            Dobot_SetPTPCmdEx(JUMP_XYZ, Coordinate[0] + blue_x_adds, Coordinate[1] + blue_y_adds, -45, 0); //The position of a target object 
                            Dobot_SetEndEffectorSuctionCupEx(true);
                            Dobot_SetPTPCmdEx(MOVL_XYZ, Coordinate[0] + blue_x_adds, Coordinate[1] + blue_y_adds, 30, 0);
                            //place Point
                            put_donw();
                            gBlue++;
                            BlockCount++;
                            printf("gBlue:");
                            Serial.println(gBlue);
                            break;
                        } else {
                            break;
                        }
                    } else {
                        break;
                    }
            }
        }   
    }
}